program test
! Test what is the optimal guiding function to use in VMC and in projector MC for a given trial wavefn.
! e = exact wavefn components
! t = trial wavefn components
! g = guiding wavefn components
! w = e*g, the distribution sampled.  In VMC make e=t and in usual VMC make e=g=t.
  use types, only: i1b, ik, rk
  implicit none
  integer, parameter:: MWALK=1000000
  integer, dimension(4) :: irand_seed
  integer nwalk, nwalk_in_psit, nwalk_outside_psit, iwalk
  real(rk) rannyu
  real(rk), parameter:: e_var=-2.
  real(rk) w(MWALK), e(MWALK), t(MWALK), g(MWALK), fraction_in_psit, g_outside_psit, &
 &wabs_gen, w2_gen, nwalk_eff, e_num, e_den, &
 &e_num_gen_ave, e_num_gen_vn1, e_den_gen_ave, e_den_gen_vn1, e_num_den_gen_vn1, &
 &e_num_genabs_ave, e_num_genabs_vn1, e_den_genabs_ave, e_den_genabs_vn1, e_num_den_genabs_vn1, &
 &e_num_gen_var, e_den_gen_var, e_num_den_gen_var, &
 &e_num_genabs_var, e_den_genabs_var, e_num_den_genabs_var, &
 &e_gen_ave, e_gen_sig, e_genabs_ave, e_genabs_sig
  real(rk) e_num_min, e_num_max
  integer n_gt_cut, n_lt_cut


  read(5,'(4i4,x,4i4)') irand_seed
  write(6,'(''random number seeds'',t25,4i4,x,4i4)') irand_seed
  read(5,*) nwalk, fraction_in_psit
  read(5,*) g_outside_psit
  if(fraction_in_psit.gt.1._rk .or. fraction_in_psit .le.0._rk) stop 'fraction_in_psit must be in (0,1]'
  if(g_outside_psit.le.0._rk) stop 'g_outside_psit must be > 0'

  call setrn(irand_seed)

  nwalk_in_psit=nint(nwalk*fraction_in_psit)
  nwalk_outside_psit=nwalk-nwalk_in_psit

  e_num_min=0; e_num_max=0; n_gt_cut=0; n_lt_cut=0

  do iwalk=1,nwalk

    if(iwalk.le.nwalk_in_psit) then
      e_den=rannyu(0)
    else
      e_den=0
    endif
    e_num=(e_var*e_den+0.5*(rannyu(0)-0.5_rk))
    e(iwalk)=sqrt(e_den)+0.0*(rannyu(0)-0.5_rk) ! set multiplying constant=0 to simulate VMC
    t(iwalk)=e_den/e(iwalk)
    g(iwalk)=abs(t(iwalk))
    !g(iwalk)=sqrt(abs(t(iwalk)))
    !g(iwalk)=1
    !g(iwalk)=max(abs(t(iwalk)),0.9)
    w(iwalk)=max(abs(e(iwalk)*g(iwalk)),0.0_rk)
    e_num=e_num/w(iwalk)
    e_den=e_den/w(iwalk)

    e_num_min=min(e_num_min,e_num)
    e_num_max=max(e_num_max,e_num)
    if(e_num.lt.-1.d2) n_lt_cut=n_lt_cut+1
    if(e_num.gt.1.d2) n_gt_cut=n_gt_cut+1

    call stats(w(iwalk), e_num, e_den, &
 &  wabs_gen, w2_gen, &
 &  e_num_gen_ave, e_num_gen_vn1, e_den_gen_ave, e_den_gen_vn1, e_num_den_gen_vn1, &
 &  e_num_genabs_ave, e_num_genabs_vn1, e_den_genabs_ave, e_den_genabs_vn1, e_num_den_genabs_vn1)

!   write(6,'(''w(iwalk),e(iwalk),t(iwalk),g(iwalk),e_num,e_den,e_num/e_den'',9f10.5)') w(iwalk),e(iwalk),t(iwalk),g(iwalk),e_num,e_den,e_num/e_den

  enddo
  nwalk_eff=wabs_gen**2/w2_gen

  e_num_gen_var=e_num_gen_vn1*nwalk_eff/(wabs_gen*(nwalk_eff-1))
  e_den_gen_var=e_den_gen_vn1*nwalk_eff/(wabs_gen*(nwalk_eff-1))
  e_num_den_gen_var=e_num_den_gen_vn1*nwalk_eff/(wabs_gen*(nwalk_eff-1))
  e_gen_ave=e_num_gen_ave/e_den_gen_ave
  e_gen_sig=sqrt(e_gen_ave**2*((e_num_gen_var/e_num_gen_ave**2)+(e_den_gen_var/e_den_gen_ave**2)-2*(e_num_den_gen_var/(e_num_gen_ave*e_den_gen_ave))))

  e_num_genabs_var=e_num_genabs_vn1*nwalk_eff/(wabs_gen*(nwalk_eff-1))
  e_den_genabs_var=e_den_genabs_vn1*nwalk_eff/(wabs_gen*(nwalk_eff-1))
  e_num_den_genabs_var=e_num_den_genabs_vn1*nwalk_eff/(wabs_gen*(nwalk_eff-1))
  e_genabs_ave=e_num_genabs_ave/e_den_genabs_ave
  e_genabs_sig=sqrt(e_genabs_ave**2*((e_num_genabs_var/e_num_genabs_ave**2)+(e_den_genabs_var/e_den_genabs_ave**2)-2*(e_num_den_genabs_var/(e_num_genabs_ave*e_den_genabs_ave))))

  write(6,'(''e_num_min, e_num_max, n_lt_cut, n_gt_cut'',2es12.4,2i9)') e_num_min, e_num_max, n_lt_cut, n_gt_cut
  write(6,'(''nwalk_eff, nwalk_abs_eff, e_den_gen_ave, e_den_gen_var, e_den_genabs_ave, e_den_genabs_var='',3f10.1,9f10.5)') nwalk_eff, nwalk*e_den_gen_ave**2/(e_den_gen_ave**2+e_den_gen_var), nwalk*e_den_genabs_ave**2/(e_den_genabs_ave**2+e_den_genabs_var), e_den_gen_ave, e_den_gen_var, e_den_genabs_ave, e_den_genabs_var
  write(6,'(''e_num_gen_ave, e_num_gen_var, e_den_gen_ave, e_den_gen_var, e_num_den_gen_var=               '',9f10.6)') e_num_gen_ave, e_num_gen_var, e_den_gen_ave, e_den_gen_var, e_num_den_gen_var
  write(6,'(''e_num_genabs_ave, e_num_genabs_var, e_den_genabs_ave, e_den_genabs_var, e_num_den_genabs_var='',9f10.6)') e_num_genabs_ave, e_num_genabs_var, e_den_genabs_ave, e_den_genabs_var, e_num_den_genabs_var
! write(6,'(''e_gen_ave(e_gen_err), e_genabs_ave(e_genabs_err)='',2(f13.6,''(''i6,'')''))') e_gen_ave, nint(10**6*e_gen_sig), e_genabs_ave, nint(10**6*e_genabs_sig)
  write(6,'(''e_gen_ave, e_gen_sig, e_genabs_ave e_genabs_sig='',2(2f13.6,3x))') e_gen_ave, e_gen_sig, e_genabs_ave, e_genabs_sig

  stop
  end

!------------------------------------------------------------------------------------------------------------
  subroutine stats(w, e_num, e_den, &
 & w_abs_sum, w2_sum, &
 & e_num_ave, e_num_vn1, e_den_ave, e_den_vn1, e_num_den_vn1, &
 & e_num_abs_ave, e_num_abs_vn1, e_den_abs_ave, e_den_abs_vn1, e_num_den_abs_vn1)
! All 3 input quantities, w, e_num, e_den, can be of either sign.
! However, for doing the statistics, we force the wts to be +ve, and absorb the sign into the rest.
! The quantities with abs in the names have smaller fluctuations but are biased because each term in the
! denominator is forced to be >=0, with the sign change (if any) being made in the numerator too.
  use types, only: i1b, ik, rk
  implicit none
  real(rk), intent(in) :: w, e_num, e_den
  real(rk), intent(inout) :: w_abs_sum, w2_sum, e_num_ave, e_num_vn1, e_den_ave, e_den_vn1, e_num_den_vn1, &
 & e_num_abs_ave, e_num_abs_vn1, e_den_abs_ave, e_den_abs_vn1, e_num_den_abs_vn1
  real(rk) w_sign, w_abs, e_num_abs, e_den_abs, e_num_del, e_den_del, e_num_abs_del, e_den_abs_del

  w_sign=sign(1._rk,w); w_abs=abs(w)
  e_num_abs=e_num*sign(1._rk,w*e_den); e_den_abs=abs(e_den)
  w_abs_sum=w_abs_sum+w_abs
  w2_sum=w2_sum+w**2

  e_num_del=w_sign*e_num-e_num_ave
  e_num_ave=e_num_ave+e_num_del*w_abs/w_abs_sum
  e_num_vn1=e_num_vn1+w_abs*(w_sign*e_num-e_num_ave)*e_num_del

  e_den_del=w_sign*e_den-e_den_ave
  e_den_ave=e_den_ave+e_den_del*w_abs/w_abs_sum
  e_den_vn1=e_den_vn1+w_abs*(w_sign*e_den-e_den_ave)*e_den_del

  e_num_den_vn1=e_num_den_vn1+w_abs*(w_sign*e_num-e_num_ave)*e_den_del

  e_num_abs_del=e_num_abs-e_num_abs_ave
  e_num_abs_ave=e_num_abs_ave+e_num_abs_del*w_abs/w_abs_sum
  e_num_abs_vn1=e_num_abs_vn1+w_abs*(e_num_abs-e_num_abs_ave)*e_num_abs_del

  e_den_abs_del=e_den_abs-e_den_abs_ave
  e_den_abs_ave=e_den_abs_ave+e_den_abs_del*w_abs/w_abs_sum
  e_den_abs_vn1=e_den_abs_vn1+w_abs*(e_den_abs-e_den_abs_ave)*e_den_abs_del

  e_num_den_abs_vn1=e_num_den_abs_vn1+w_abs*(e_num_abs-e_num_abs_ave)*e_den_abs_del

  return
  end subroutine stats
