program test
  use types, only: i1b, ik, rk
! implicit none
  implicit real*8(a-h,o-z)
  real(rk) w(100000), e_num(100000) ,e_den(100000)

  iseed=3215; call random_seed(iseed)

  nwalk=100 ; nstep=10; nblk=100

  w_blk=0; e_num_blk_ave=0; e_num_blk_vn1=0; e_den_blk_ave=0; e_den_blk_vn1=0
  e_num_den_blk_ave=0; e_num_den_blk_vn1=0
  w_genabs_blk=0; e_num_genabs_blk_ave=0; e_num_genabs_blk_vn1=0; e_den_genabs_blk_ave=0; e_den_genabs_blk_vn1=0
  e_num_den_genabs_blk_ave=0; e_num_den_genabs_blk_vn1=0
  do istep=1,nstep

    w_gen=0; e_num_gen_ave=0; e_num_gen_vn1=0; e_den_gen_ave=0; e_den_gen_vn1=0
    e_num_den_gen_ave=0; e_num_den_gen_vn1=0
    w_abs_gen=0; e_num_abs_gen_ave=0; e_num_abs_gen_vn1=0; e_den_abs_gen_ave=0; e_den_abs_gen_vn1=0
    e_num_den_abs_gen_ave=0; e_num_den_abs_gen_vn1=0
    do iwalk=1,nwalk
      call random_number(ran) ! just to get different numbers
      call random_number(ran); w(iwalk)=ran-.5
      call random_number(ran); e_num(iwalk)=-2.5+ran
      call random_number(ran); e_den(iwalk)=0.5+ran

      call stats(w(iwalk), e_num(iwalk), e_den(iwalk), &
   &  w_gen, w_abs_gen, e_num_gen_ave, e_num_gen_vn1, e_den_gen_ave, e_den_gen_vn1, e_num_den_gen_vn1, &
   &  e_num_abs_gen_ave, e_num_abs_gen_vn1, e_den_abs_gen_ave, e_den_abs_gen_vn1, e_num_den_abs_gen_vn1)
    enddo
    e_num_gen_var=e_num_gen_vn1*nwalk/(w_gen*(nwalk-1))
    e_den_gen_var=e_den_gen_vn1*nwalk/(w_gen*(nwalk-1))
    e_num_den_gen_var=e_num_den_gen_vn1*nwalk/(w_gen*(nwalk-1))
    e_num_abs_gen_var=e_num_abs_gen_vn1*nwalk/(w_abs_gen*(nwalk-1))
    e_den_abs_gen_var=e_den_abs_gen_vn1*nwalk/(w_abs_gen*(nwalk-1))
    e_num_den_abs_gen_var=e_num_den_abs_gen_vn1*nwalk/(w_abs_gen*(nwalk-1))
    write(6,'(''nwalk_eff, nwalk_abs_eff, e_den_gen_ave, e_den_gen_var, e_den_abs_gen_ave, e_den_abs_gen_var='',2f10.1,9f10.5)') nwalk*e_den_gen_ave**2/(e_den_gen_ave**2+e_den_gen_var), nwalk*e_den_abs_gen_ave**2/(e_den_abs_gen_ave**2+e_den_abs_gen_var), e_den_gen_ave, e_den_gen_var, e_den_abs_gen_ave, e_den_abs_gen_var
    write(6,'(''e_num_gen_ave, e_num_gen_var, e_den_gen_ave, e_den_gen_var, e_num_den_gen_var=                    '',9f10.6)') e_num_gen_ave, e_num_gen_var, e_den_gen_ave, e_den_gen_var, e_num_den_gen_var
    write(6,'(''e_num_abs_gen_ave, e_num_abs_gen_var, e_den_abs_gen_ave, e_den_abs_gen_var, e_num_den_abs_gen_var='',9f10.6)') e_num_abs_gen_ave, e_num_abs_gen_var, e_den_abs_gen_ave, e_den_abs_gen_var, e_num_den_abs_gen_var

    call stats(w_gen, e_num_gen_ave, e_den_gen_ave, &
 &  w_blk, w_genabs_blk, e_num_blk_ave, e_num_blk_vn1, e_den_blk_ave, e_den_blk_vn1, e_num_den_blk_vn1, &
 &  e_num_genabs_blk_ave, e_num_genabs_blk_vn1, e_den_genabs_blk_ave, e_den_genabs_blk_vn1, e_num_den_genabs_blk_vn1)
  enddo
  e_num_blk_var=e_num_blk_vn1*nstep/(w_blk*(nstep-1))
  e_den_blk_var=e_den_blk_vn1*nstep/(w_blk*(nstep-1))
  e_num_den_blk_var=e_num_den_blk_vn1*nstep/(w_blk*(nstep-1))
  e_num_genabs_blk_var=e_num_genabs_blk_vn1*nstep/(w_genabs_blk*(nstep-1))
  e_den_genabs_blk_var=e_den_genabs_blk_vn1*nstep/(w_genabs_blk*(nstep-1))
  e_num_den_genabs_blk_var=e_num_den_genabs_blk_vn1*nstep/(w_genabs_blk*(nstep-1))
  write(6,'(''e_num_blk_ave, e_num_blk_var, e_den_blk_ave, e_den_blk_var, e_num_den_blk_var                                   '',9f10.6)') e_num_blk_ave, e_num_blk_var, e_den_blk_ave, e_den_blk_var, e_num_den_blk_var
  write(6,'(''e_num_genabs_blk_ave, e_num_genabs_blk_var, e_den_genabs_blk_ave, e_den_genabs_blk_var, e_num_den_genabs_blk_var'',9f10.6)') e_num_genabs_blk_ave, e_num_genabs_blk_var, e_den_genabs_blk_ave, e_den_genabs_blk_var, e_num_den_genabs_blk_var

  stop
  end

!------------------------------------------------------------------------------------------------------------
  subroutine stats(w, e_num, e_den, &
 & w_sum, w_abs_sum, e_num_ave, e_num_vn1, e_den_ave, e_den_vn1, e_num_den_vn1, &
 & e_num_abs_ave, e_num_abs_vn1, e_den_abs_ave, e_den_abs_vn1, e_num_den_abs_vn1)
  use types, only: i1b, ik, rk
  implicit none
  real(rk), intent(in) :: w, e_num, e_den
  real(rk), intent(inout) :: w_sum, w_abs_sum, e_num_ave, e_num_vn1, e_den_ave, e_den_vn1, e_num_den_vn1, &
 & e_num_abs_ave, e_num_abs_vn1, e_den_abs_ave, e_den_abs_vn1, e_num_den_abs_vn1
  real(rk) w_sign, w_abs, e_num_abs, e_den_abs, e_num_del, e_den_del, e_num_abs_del, e_den_abs_del

  w_sign=sign(1._rk,w); w_abs=abs(w)
  e_num_abs=e_num*sign(1._rk,w*e_den); e_den_abs=abs(e_den)
  w_abs_sum=w_abs_sum+w_abs
  w_sum=w_sum+w

  e_num_del=w_sign*e_num-e_num_ave
  e_num_ave=e_num_ave+e_num_del*w_abs/w_abs_sum
  e_num_vn1=e_num_vn1+w_abs*(w_sign*e_num-e_num_ave)*e_num_del

  e_den_del=w_sign*e_den-e_den_ave
  e_den_ave=e_den_ave+e_den_del*w_abs/w_abs_sum
  e_den_vn1=e_den_vn1+w_abs*(w_sign*e_den-e_den_ave)*e_den_del

  e_num_den_vn1=e_num_den_vn1+w_abs*(w_sign*e_num-e_num_ave)*e_den_del

  e_num_abs_del=e_num_abs-e_num_abs_ave
  e_num_abs_ave=e_num_abs_ave+e_num_abs_del*w_abs/w_abs_sum
  e_num_abs_vn1=e_num_abs_vn1+w_abs*(e_num_abs-e_num_abs_ave)*e_num_abs_del

  e_den_abs_del=e_den_abs-e_den_abs_ave
  e_den_abs_ave=e_den_abs_ave+e_den_abs_del*w_abs/w_abs_sum
  e_den_abs_vn1=e_den_abs_vn1+w_abs*(e_den_abs-e_den_abs_ave)*e_den_abs_del

  e_num_den_abs_vn1=e_num_den_abs_vn1+w_abs*(e_num_abs-e_num_abs_ave)*e_den_abs_del

  return
  end subroutine stats
