program template2

  integer ipr

  call walk

!------------------------------------------------------------------------------------------------------------

  contains

  subroutine walk
! This routine demonstrates various things:
! 1) Welford's algorithm for calculating standard deviations
! 2) Comparison of split-join versus comb algorithms for keeping deviations of walker weights small
! 3) Algorithm for correcting for population control
! 4) Comparison of blocking versus correlation function method for estimating the autocorrelation time
! 5) When there is a sign problem, the reduction in statistical error (accompanied by a bias) from using
!    a sum of ratios rather than a ratio of sums, at a) the walker level, b) the generation level, c) the block level.
  use types, only: i1b, i8b, ik, rk
  implicit none

  integer, dimension(4) :: irand_seed
  integer, parameter :: MWALK=2000000, n_pop_cntrl_corr=21
  real(rk) rannyu, noise, fraction_negative_wt, e_num_prev, e_den_prev, p, q, e_trial, e_loc, e_est, tau, reweight_factor_inv, reweight_factor_prod(n_pop_cntrl_corr), reweight_factor_pow(n_pop_cntrl_corr), &
  w_abs_gen, w_abs_gen_imp, w_perm_initiator_gen, &
  w_blk, w_abs_blk, &
  w_abs_run, ran
  integer(i8b)  passes, nwalk_passes, nwalk_blk, nwalk_run
  integer :: i, i_pop_cntrl_corr_best=0

  integer iwalk, istep, iblk, iblkk, nwalk_target, nwalk, nstep, nblk, nblk_eq, ntimes_nblk_eq

  real(rk) &
 &    e_num_wlk, e_den_wlk, & ! next 4 lines for wlk; do not need pop_cntrl subscript
 &    e_num_wlk_ave, e_num_wlk_vn1, e_num_wlk_var, e_den_wlk_ave, e_den_wlk_vn1, e_den_wlk_var, e_num_den_wlk_vn1, e_num_den_wlk_var, &
 &    e_num_wlkabs_wlk, e_den_wlkabs_wlk, &
 &    e_num_wlkabs_wlk_ave, e_num_wlkabs_wlk_vn1, e_num_wlkabs_wlk_var, e_den_wlkabs_wlk_ave, e_den_wlkabs_wlk_vn1, e_den_wlkabs_wlk_var, e_num_den_wlkabs_wlk_vn1, e_num_den_wlkabs_wlk_var, &
!&    e_num_genabs_wlk_ave, e_den_genabs_wlk_ave, e_gen, e_gen_del, e_gen_ave, e_gen_vn1, e_gen_var, e_genp_e_gen_vn1, e_genp_e_gen_var, &
 &    e_num_gen(n_pop_cntrl_corr), e_den_gen(n_pop_cntrl_corr), & ! next 6 lines for gen
 &    e_num_gen_ave(n_pop_cntrl_corr), e_num_gen_vn1(n_pop_cntrl_corr), e_num_gen_var(n_pop_cntrl_corr), e_den_gen_ave(n_pop_cntrl_corr), e_den_gen_vn1(n_pop_cntrl_corr), e_den_gen_var(n_pop_cntrl_corr), e_num_den_gen_vn1(n_pop_cntrl_corr), e_num_den_gen_var(n_pop_cntrl_corr), &
 &    e_num_wlkabs_gen(n_pop_cntrl_corr), e_den_wlkabs_gen(n_pop_cntrl_corr), &
 &    e_num_wlkabs_gen_ave(n_pop_cntrl_corr), e_num_wlkabs_gen_vn1(n_pop_cntrl_corr), e_num_wlkabs_gen_var(n_pop_cntrl_corr), e_den_wlkabs_gen_ave(n_pop_cntrl_corr), e_den_wlkabs_gen_vn1(n_pop_cntrl_corr), e_den_wlkabs_gen_var(n_pop_cntrl_corr), e_num_den_wlkabs_gen_vn1(n_pop_cntrl_corr), e_num_den_wlkabs_gen_var(n_pop_cntrl_corr), &
 &    e_num_genabs_gen(n_pop_cntrl_corr), e_den_genabs_gen(n_pop_cntrl_corr), &
 &    e_num_genabs_gen_ave(n_pop_cntrl_corr), e_num_genabs_gen_vn1(n_pop_cntrl_corr), e_num_genabs_gen_var(n_pop_cntrl_corr), e_den_genabs_gen_ave(n_pop_cntrl_corr), e_den_genabs_gen_vn1(n_pop_cntrl_corr), e_den_genabs_gen_var(n_pop_cntrl_corr), e_num_den_genabs_gen_vn1(n_pop_cntrl_corr), e_num_den_genabs_gen_var(n_pop_cntrl_corr), &
 &    e_num_blk(n_pop_cntrl_corr), e_den_blk(n_pop_cntrl_corr), & ! next 8 lines for blk
 &    e_num_blk_ave(n_pop_cntrl_corr), e_num_blk_vn1(n_pop_cntrl_corr), e_num_blk_var(n_pop_cntrl_corr), e_den_blk_ave(n_pop_cntrl_corr), e_den_blk_vn1(n_pop_cntrl_corr), e_den_blk_var(n_pop_cntrl_corr), e_num_den_blk_vn1(n_pop_cntrl_corr), e_num_den_blk_var(n_pop_cntrl_corr), &
 &    e_num_wlkabs_blk(n_pop_cntrl_corr), e_den_wlkabs_blk(n_pop_cntrl_corr), &
 &    e_num_wlkabs_blk_ave(n_pop_cntrl_corr), e_num_wlkabs_blk_vn1(n_pop_cntrl_corr), e_num_wlkabs_blk_var(n_pop_cntrl_corr), e_den_wlkabs_blk_ave(n_pop_cntrl_corr), e_den_wlkabs_blk_vn1(n_pop_cntrl_corr), e_den_wlkabs_blk_var(n_pop_cntrl_corr), e_num_den_wlkabs_blk_vn1(n_pop_cntrl_corr), e_num_den_wlkabs_blk_var(n_pop_cntrl_corr), &
 &    e_num_genabs_blk(n_pop_cntrl_corr), e_den_genabs_blk(n_pop_cntrl_corr), &
 &    e_num_genabs_blk_ave(n_pop_cntrl_corr), e_num_genabs_blk_vn1(n_pop_cntrl_corr), e_num_genabs_blk_var(n_pop_cntrl_corr), e_den_genabs_blk_ave(n_pop_cntrl_corr), e_den_genabs_blk_vn1(n_pop_cntrl_corr), e_den_genabs_blk_var(n_pop_cntrl_corr), e_num_den_genabs_blk_vn1(n_pop_cntrl_corr), e_num_den_genabs_blk_var(n_pop_cntrl_corr), &
 &    e_num_blkabs_blk(n_pop_cntrl_corr), e_den_blkabs_blk(n_pop_cntrl_corr), &
 &    e_num_blkabs_blk_ave(n_pop_cntrl_corr), e_num_blkabs_blk_vn1(n_pop_cntrl_corr), e_num_blkabs_blk_var(n_pop_cntrl_corr), e_den_blkabs_blk_ave(n_pop_cntrl_corr), e_den_blkabs_blk_vn1(n_pop_cntrl_corr), e_den_blkabs_blk_var(n_pop_cntrl_corr), e_num_den_blkabs_blk_vn1(n_pop_cntrl_corr), e_num_den_blkabs_blk_var(n_pop_cntrl_corr)

! real(rk) e_wlkabs_gen_ave, e_genabs_gen_ave, e_gen_err, e_wlkabs_gen_err, e_genabs_gen_err
  real(rk) e_wlkabs_wlk_ave, e_wlkabs_wlk_err, e_wlkabs_gen_ave(n_pop_cntrl_corr), e_wlkabs_gen_err(n_pop_cntrl_corr) , e_wlkabs_blk_ave(n_pop_cntrl_corr), e_wlkabs_blk_err(n_pop_cntrl_corr), &
 & e_genabs_gen_ave(n_pop_cntrl_corr), e_genabs_gen_err(n_pop_cntrl_corr) , e_genabs_blk_ave(n_pop_cntrl_corr), e_genabs_blk_err(n_pop_cntrl_corr), &
 & e_blkabs_blk_ave(n_pop_cntrl_corr), e_blkabs_blk_err(n_pop_cntrl_corr)

  real(rk) e_wlk_ave, e_wlk_err, &
 & e_gen_ave(n_pop_cntrl_corr), e_gen_err(n_pop_cntrl_corr), &
 & e_blk_ave(n_pop_cntrl_corr), e_blk_err(n_pop_cntrl_corr), &
 & e_blkabs_blk_ave_best, e_blkabs_blk_err_best, e_change, e_slope, e_slope_prev, &
 & e_genpp, e_genpp_del, e_genpp_ave, e_genpp_vn1, e_genpp_e_genp_vn1, &
 & e_genp, e_genp_ave
  real(rk) tau_corr_nonint, t_corr_nonint, t_corr, &
  walk_wt(MWALK), e_num_w(MWALK), e_den_w(MWALK), &
  e_num_true, e_den_true, population_control_exponent, t_corr_est
  
  character*16 method

  e_num_prev=-2; e_den_prev=1; e_num_true=-2; e_den_true=1; e_trial=-2

  write(6,'(''template2'')')
  read(5,*) ipr
  write(6,'(''ipr='',i3)') ipr
  read(5,'(4i4,x,4i4)') irand_seed
  write(6,'(''random number seeds'',t25,4i4,x,4i4)') irand_seed
  call setrn(irand_seed)
  read(5,*) nstep, nblk, nblk_eq
  read(5,*) nwalk_target
  read(5,*) tau, t_corr_est, population_control_exponent
  read(5,*) noise, fraction_negative_wt
  read(5,*) method
  write(6,'(''nstep, nblk, nblk_eq='',9i8)') nstep, nblk, nblk_eq
  write(6,'(''nwalk_target='',9i8)') nwalk_target
  write(6,'(''tau, t_corr_est, population_control_exponent='',9f10.5)') tau, t_corr_est, population_control_exponent
  write(6,'(''noise, fraction_negative_wt='',9f10.5)') noise, fraction_negative_wt
  write(6,'(''If noise>2, e_den will be of both signs; if noise>4, e_den will be of both signs'',/)')
  write(6,'(''method='',a)') method
  if(fraction_negative_wt.lt.0._rk .or. fraction_negative_wt.gt..5_rk) stop 'fraction_negative_wt must be between 0 and .5'
  p=exp(-tau/t_corr_est)
  q=1-p
  reweight_factor_inv=1
  reweight_factor_pow(1)=0
  do i=2,n_pop_cntrl_corr
    reweight_factor_pow(i)=exp(-1/sqrt(2._rk)**(i-2))
    reweight_factor_prod(i)=1
  enddo
  write(6,'(/,''reweight_factor_pow='',100f8.3)') (reweight_factor_pow(i),i=1,n_pop_cntrl_corr)
  write(6,'(''Number of generations='',100f8.1,/)') (-1/log(reweight_factor_pow(i)),i=1,n_pop_cntrl_corr)

  if(nwalk.gt.MWALK) stop 'nwalk>MWALK'
  nwalk=nwalk_target

  e_est=e_num_true/e_den_true
  w_abs_gen=nwalk

  do iwalk=1,nwalk
    e_num_w(iwalk)=e_num_true+(rannyu(0)-0.5_rk)*noise
    e_den_w(iwalk)=e_den_true+(rannyu(0)-0.5_rk)*noise
    walk_wt(iwalk)=1
  enddo
  if(ipr.ge.1) then
    write(6,'(100f10.5)') e_num_w(1:nwalk)
    write(6,'(100f10.5)') e_den_w(1:nwalk)
  endif

  call my_second(1,'run')

  write(6,'(/,''iblk, e_blk_ave(err)          e_blk_ave_best(err)  t_corr_nonint t_corr'')')

! Walk the walk
  ntimes_nblk_eq=2
  do iblkk=1,ntimes_nblk_eq*nblk_eq+nblk

! The number of equilibration blocks is ntimes_nblk_eq * nblk_eq.  Before each set of nblk_eq equilibration blocks, set estimators to 0.
    if(mod(iblkk,nblk_eq).eq.1 .and. iblkk.le.ntimes_nblk_eq*nblk_eq+1) then
      nwalk_run=0; w_abs_run=0
      e_genpp_ave=0; e_genpp_vn1=0; e_genpp_e_genp_vn1=0;
      nwalk_passes=0
      e_num_wlk_ave=0; e_num_wlk_vn1=0; e_den_wlk_ave=0; e_den_wlk_vn1=0; e_num_den_wlk_vn1=0
      e_num_wlkabs_wlk_ave=0; e_num_wlkabs_wlk_vn1=0; e_den_wlkabs_wlk_ave=0; e_den_wlkabs_wlk_vn1=0; e_num_den_wlkabs_wlk_vn1=0
      e_num_wlkabs_gen_ave=0; e_num_wlkabs_gen_vn1=0; e_den_wlkabs_gen_ave=0; e_den_wlkabs_gen_vn1=0; e_num_den_wlkabs_gen_vn1=0
      e_num_wlkabs_blk_ave=0; e_num_wlkabs_blk_vn1=0; e_den_wlkabs_blk_ave=0; e_den_wlkabs_blk_vn1=0; e_num_den_wlkabs_blk_vn1=0
      e_num_gen_ave=0; e_num_gen_vn1=0; e_den_gen_ave=0; e_den_gen_vn1=0; e_num_den_gen_vn1=0
      e_num_genabs_gen_ave=0; e_num_genabs_gen_vn1=0; e_den_genabs_gen_ave=0; e_den_genabs_gen_vn1=0; e_num_den_genabs_gen_vn1=0
      e_num_genabs_blk_ave=0; e_num_genabs_blk_vn1=0; e_den_genabs_blk_ave=0; e_den_genabs_blk_vn1=0; e_num_den_genabs_blk_vn1=0
      e_num_blk_ave=0; e_num_blk_vn1=0; e_den_blk_ave=0; e_den_blk_vn1=0; e_num_den_blk_vn1=0
      e_num_blkabs_blk_ave=0; e_num_blkabs_blk_vn1=0; e_den_blkabs_blk_ave=0; e_den_blkabs_blk_vn1=0; e_num_den_blkabs_blk_vn1=0
      i_pop_cntrl_corr_best=1
    endif

    if(iblkk.le.ntimes_nblk_eq*nblk_eq) then
      iblk=mod(iblkk-1,nblk_eq)+1
    else
      iblk=iblkk-ntimes_nblk_eq*nblk_eq
    endif

    nwalk_blk=0 ; w_abs_blk=0 ; w_blk=0
    e_num_blk=0; e_den_blk=0
    e_num_wlkabs_blk=0; e_den_wlkabs_blk=0
    e_num_genabs_blk=0; e_den_genabs_blk=0

    do istep=1,nstep

      reweight_factor_inv = (1+tau*(e_trial-e_est))*(dfloat(nwalk_target)/abs(w_abs_gen))**min(1.d0,tau*population_control_exponent)
      if(reweight_factor_inv.lt.0) then
        write(6,'(''Warning: reweight_factor_inv, e_trial, e_est, w_abs_gen, population_control_exponent'',9es12.4)') reweight_factor_inv, e_trial, e_est, w_abs_gen, population_control_exponent
        reweight_factor_inv = min(10.d0, max(1.d-1,reweight_factor_inv))
        !stop 'reweight_factor_inv.lt.0'
      endif
      do i=1,n_pop_cntrl_corr
! Warning: tmp.  Now restored
!       reweight_factor_prod(i)=(reweight_factor_prod(i)**reweight_factor_pow(i))/reweight_factor_inv
        reweight_factor_prod(i)=(reweight_factor_prod(i)/reweight_factor_inv)**reweight_factor_pow(i)
      enddo
      !write(6,'(''reweight_factor_prod='',100es11.3)') 1/reweight_factor_inv, (reweight_factor_prod(i),i=1,n_pop_cntrl_corr)

      if(e_den_blkabs_blk_ave(1).ne.0) then
        e_est=e_num_blkabs_blk_ave(1)/e_den_blkabs_blk_ave(1)
        e_est=min(e_est, e_num_blkabs_blk_ave(i_pop_cntrl_corr_best)/e_den_blkabs_blk_ave(i_pop_cntrl_corr_best))
! We want to put e_trial=e_est but we don't want to make very large changes since at an early stage e_est may be far off.
! So, change e_trial by e_est-e_trial, but bound the change to be within [-1,1].
        if(iblkk.le.ntimes_nblk_eq*nblk_eq) then
          e_trial=e_trial+sign(min(abs(e_est-e_trial),1._rk),e_est-e_trial) ! Warning: when we remove population control error, we should not update e_trial after first equilibration is done
        endif
        if(ipr.ge.1 .and. iblk.ge.3 .and. iblkk.le.ntimes_nblk_eq*nblk_eq .and. istep.eq.nstep) write(6,'(''At end of block'',i4,'', e_trial='',9es13.5)') iblkk, e_trial, e_num_blkabs_blk_ave(i_pop_cntrl_corr_best)/e_den_blkabs_blk_ave(i_pop_cntrl_corr_best)
        if(iblkk.eq.ntimes_nblk_eq*nblk_eq .and. istep.eq.nstep) write(6,'(''At end of equil, e_trial='',9es13.5)') e_trial
        w_abs_gen=sum(abs(walk_wt(1:nwalk)))
        if(ipr.ge.1 .or. (ipr.ge.0.and.iblkk.le.ntimes_nblk_eq*nblk_eq)) write(6,'(''iblkk, istep, w_perm_initiator_gen, nwalk, w_abs_gen, w_abs_gen_imp, e_gen, e_est, e_trial, reweight_factor_inv, t_c='',2i6,f8.1,i9,f11.1,f10.1,f10.4,2f11.5,f10.5,f8.1)') &
 &      iblkk, istep, w_perm_initiator_gen, nwalk, w_abs_gen, w_abs_gen_imp, e_num_gen(1)/e_den_gen(1), e_est, e_trial, reweight_factor_inv, t_corr_nonint
        call flush(6)
      endif

      e_num_gen(1)=0; e_den_gen(1)=0
      e_num_wlkabs_gen(1)=0; e_den_wlkabs_gen(1)=0

      do iwalk=1,nwalk
        e_loc=p*(e_num_w(iwalk)/e_den_w(iwalk))+q*(e_num_true/e_den_true)*(1+2*(rannyu(0)-0.5_rk)*noise)
        e_num_w(iwalk)=p*e_num_w(iwalk)+q*e_num_true+(1+2*(rannyu(0)-0.5_rk)*noise)
        e_den_w(iwalk)=e_num_w(iwalk)/e_loc
        walk_wt(iwalk)=walk_wt(iwalk)*min(max((1+tau*(e_trial-e_loc)),0.1),10._rk)*reweight_factor_inv
! Allow walk_wt to be negative at times
        call random_number(ran)
! A fraction fraction_negative_wt are negative, regardless of their previous weight or sign
!       walk_wt(iwalk)=abs(walk_wt(iwalk))*sign(1._rk,ran-fraction_negative_wt)
! A fraction fraction_negative_wt/2 are negative if previous weight was positive, a fraction 3*fraction_negative_wt/2 are negative if previous weight was negative
        walk_wt(iwalk)=abs(walk_wt(iwalk))*sign(1._rk,ran+fraction_negative_wt*(sign(.5_rk,walk_wt(iwalk))-1._rk))
      enddo

      if(method.eq.'split_join') then
        call split_join_signed(walk_wt, e_num_w, e_den_w, nwalk, MWALK)
      elseif(method.eq.'comb') then
        call comb_signed(walk_wt, e_num_w, e_den_w, nwalk)
      else
        stop 'method must be split_join or comb'
      endif

      do iwalk=1,nwalk
        nwalk_passes=nwalk_passes+1

        e_num_wlk=walk_wt(iwalk)*e_num_w(iwalk)
        e_den_wlk=walk_wt(iwalk)*e_den_w(iwalk)

        e_num_wlkabs_wlk=e_num_wlk*sign(1._rk,e_den_wlk)
        e_den_wlkabs_wlk=abs(e_den_wlk)

!       if(e_num_wlk.gt.0 .or. e_den_wlk.lt.0) write(6,'(''Warning: e_num_wlk, e_den_wlk='',9f12.4)') e_num_wlk, e_den_wlk

        e_num_gen(1)=e_num_gen(1)+e_num_wlk
        e_den_gen(1)=e_den_gen(1)+e_den_wlk
        e_num_wlkabs_gen(1)=e_num_wlkabs_gen(1)+e_num_wlkabs_wlk
        e_den_wlkabs_gen(1)=e_den_wlkabs_gen(1)+e_den_wlkabs_wlk

        call stats(1, nwalk_passes, e_num_wlk, e_den_wlk, &
 &      e_num_wlk_ave, e_num_wlk_vn1, e_den_wlk_ave, e_den_wlk_vn1, e_num_den_wlk_vn1)
        call stats(1, nwalk_passes, e_num_wlkabs_wlk, e_den_wlkabs_wlk, &
 &      e_num_wlkabs_wlk_ave, e_num_wlkabs_wlk_vn1, e_den_wlkabs_wlk_ave, e_den_wlkabs_wlk_vn1, e_num_den_wlkabs_wlk_vn1)
      enddo ! iwalk

!     write(6,'(''e_num_wlk_ave(e_num_wlk_err)='',2(2es16.8,''(''i8,'')''))') e_num_wlk, e_num_wlk_ave, int(sqrt(e_num_wlk_vn1)/nwalk)
!     write(6,'(''e_num_wlkabs_wlk_ave(e_num_wlkabs_wlk_err)='',2(2es16.8,''(''i8,'')''))') e_num_wlkabs_wlk, e_num_wlkabs_wlk_ave, int(sqrt(e_num_wlkabs_wlk_vn1)/nwalk)

      passes=(iblk-1)*nstep+istep

      nwalk_blk=nwalk_blk+nwalk
      w_abs_gen=sum(abs(walk_wt(1:nwalk)))
      w_abs_blk=w_abs_blk+w_abs_gen

! Partially remove population control bias
      do i=1,n_pop_cntrl_corr
        e_num_gen(i)=reweight_factor_prod(i)*e_num_gen(1)
        e_den_gen(i)=reweight_factor_prod(i)*e_den_gen(1)
        e_num_wlkabs_gen(i)=reweight_factor_prod(i)*e_num_wlkabs_gen(1)
        e_den_wlkabs_gen(i)=reweight_factor_prod(i)*e_den_wlkabs_gen(1)
        e_num_genabs_gen(i)=e_num_gen(i)*sign(1._rk,e_den_gen(i))
        e_den_genabs_gen(i)=abs(e_den_gen(i))
      enddo

      call stats(n_pop_cntrl_corr, passes, e_num_gen, e_den_gen, &
 &    e_num_gen_ave, e_num_gen_vn1, e_den_gen_ave, e_den_gen_vn1, e_num_den_gen_vn1)
      call stats(n_pop_cntrl_corr, passes, e_num_wlkabs_gen, e_den_wlkabs_gen, &
 &    e_num_wlkabs_gen_ave, e_num_wlkabs_gen_vn1, e_den_wlkabs_gen_ave, e_den_wlkabs_gen_vn1, e_num_den_wlkabs_gen_vn1)
      call stats(n_pop_cntrl_corr, passes, e_num_genabs_gen, e_den_genabs_gen, &
 &    e_num_genabs_gen_ave, e_num_genabs_gen_vn1, e_den_genabs_gen_ave, e_den_genabs_gen_vn1, e_num_den_genabs_gen_vn1)

      e_num_blk=e_num_blk+e_num_gen; e_den_blk=e_den_blk+e_den_gen
      e_num_wlkabs_blk=e_num_wlkabs_blk+e_num_wlkabs_gen; e_den_wlkabs_blk=e_den_wlkabs_blk+e_den_wlkabs_gen
      e_num_genabs_blk=e_num_genabs_blk+e_num_genabs_gen; e_den_genabs_blk=e_den_genabs_blk+e_den_genabs_gen
      e_num_blkabs_blk=e_num_blk*sign(1._rk,e_den_blk); e_den_blkabs_blk=abs(e_den_blk) ! This could be moved outside istep loop

! e_genpp is the energy of the current generation and e_genp of the previous one
      e_genpp=e_num_gen(1)/e_den_gen(1)
      e_genpp_del=e_genpp-e_genpp_ave
      e_genpp_ave=e_genpp_ave+e_genpp_del/(passes)
      e_genpp_vn1=e_genpp_vn1+(e_genpp-e_genpp_ave)*e_genpp_del
      if(passes.ge.2) then
        e_genpp_e_genp_vn1=e_genpp_e_genp_vn1+(e_genp-e_genp_ave)*e_genpp_del
        if(passes.ge.3 .and. e_genpp_e_genp_vn1.gt.0) then
          tau_corr_nonint=-1/log(((passes-1)/(passes-2))*e_genpp_e_genp_vn1/e_genpp_vn1)
        else
          tau_corr_nonint=0
        endif
        t_corr_nonint=1+2*tau_corr_nonint
!       if(e_genpp_e_genp_vn1.le.0.d0 .or. e_genpp_vn1.le.0.d0 .or. e_genpp_e_genp_vn1.gt.e_genpp_vn1) write(6,'(''e_genp_e_gen_vn1, e_gen_vn1='',9es12.4)') e_genpp_e_genp_vn1, e_genpp_vn1
      endif
      e_genp=e_genpp
      e_genp_ave=e_genpp_ave

!     if(istep.ge.2) then
!       e_gen_ave=e_num_gen_ave(1)/e_den_gen_ave(1)
!       e_wlkabs_gen_ave=e_num_wlkabs_gen_ave(1)/e_den_wlkabs_gen_ave(1)
!       e_genabs_gen_ave=e_num_genabs_gen_ave(1)/e_den_genabs_gen_ave(1)
!       e_gen_err=sqrt(e_gen_ave**2*(e_num_gen_vn1(1)/e_num_gen_ave(1)**2+(e_den_gen_vn1(1)/e_den_gen_ave(1)**2-2*e_num_den_gen_vn1(1)/(e_num_gen_ave(1)*e_den_gen_ave(1))))/(istep-1))
!       e_wlkabs_gen_err=sqrt(e_wlkabs_gen_ave**2*(e_num_wlkabs_gen_vn1(1)/e_num_wlkabs_gen_ave(1)**2+(e_den_wlkabs_gen_vn1(1)/e_den_wlkabs_gen_ave(1)**2-2*e_num_den_wlkabs_gen_vn1(1)/(e_num_wlkabs_gen_ave(1)*e_den_wlkabs_gen_ave(1))))/(istep-1))
!       e_genabs_gen_err=sqrt(e_genabs_gen_ave**2*(e_num_genabs_gen_vn1(1)/e_num_genabs_gen_ave(1)**2+(e_den_genabs_gen_vn1(1)/e_den_genabs_gen_ave(1)**2-2*e_num_den_genabs_gen_vn1(1)/(e_num_genabs_gen_ave(1)*e_den_genabs_gen_ave(1))))/(istep-1))

!       write(6,'(''e_gen_ave(e_gen_err)='',100(f13.8,''(''i8,'')''))') e_gen_ave,nint(e_gen_err)
!       write(6,'(''e_wlkabs_gen_ave(e_wlkabs_gen_err)='',100(f13.8,''(''i8,'')''))') e_wlkabs_gen_ave,nint(e_wlkabs_gen_err)
!       write(6,'(''e_genabs_gen_ave(e_genabs_gen_err)='',100(f13.8,''(''i8,'')''))') e_genabs_gen_ave,nint(e_genabs_gen_err)
!     endif

    enddo ! istep

    nwalk_run=nwalk_run+nwalk_blk
    w_abs_run=w_abs_run+w_abs_blk
    !w_abs_blk2_run=w_abs_blk2_run+w_abs_blk**2

!   e_num_blk=nstep*e_num_gen_ave; e_den_blk=nstep*e_den_gen_ave
!   e_num_wlkabs_blk=nstep*e_num_wlkabs_gen_ave; e_den_wlkabs_blk=nstep*e_den_wlkabs_gen_ave
!   e_num_genabs_blk=nstep*e_num_genabs_gen_ave; e_den_genabs_blk=nstep*e_den_genabs_gen_ave
!   e_num_blkabs_blk=e_num_blk*sign(1._rk,e_den_blk); e_den_blkabs_blk=abs(e_den_blk)

    call stats(n_pop_cntrl_corr, int(iblk,i8b), e_num_blk, e_den_blk, &
 &  e_num_blk_ave, e_num_blk_vn1, e_den_blk_ave, e_den_blk_vn1, e_num_den_blk_vn1)
    call stats(n_pop_cntrl_corr, int(iblk,i8b), e_num_wlkabs_blk, e_den_wlkabs_blk, &
 &  e_num_wlkabs_blk_ave, e_num_wlkabs_blk_vn1, e_den_wlkabs_blk_ave, e_den_wlkabs_blk_vn1, e_num_den_wlkabs_blk_vn1)
    call stats(n_pop_cntrl_corr, int(iblk,i8b), e_num_genabs_blk, e_den_genabs_blk, &
 &  e_num_genabs_blk_ave, e_num_genabs_blk_vn1, e_den_genabs_blk_ave, e_den_genabs_blk_vn1, e_num_den_genabs_blk_vn1)
    call stats(n_pop_cntrl_corr, int(iblk,i8b), e_num_blkabs_blk, e_den_blkabs_blk, &
 &  e_num_blkabs_blk_ave, e_num_blkabs_blk_vn1, e_den_blkabs_blk_ave, e_den_blkabs_blk_vn1, e_num_den_blkabs_blk_vn1)

    if(iblk.ge.2) then
      e_num_wlk_var=e_num_wlk_vn1/(nwalk_passes-1); e_den_wlk_var=e_den_wlk_vn1/(nwalk_passes-1); e_num_den_wlk_var=e_num_den_wlk_vn1/(nwalk_passes-1)
      e_num_wlkabs_wlk_var=e_num_wlkabs_wlk_vn1/(nwalk_passes-1); e_den_wlkabs_wlk_var=e_den_wlkabs_wlk_vn1/(nwalk_passes-1); e_num_den_wlkabs_wlk_var=e_num_den_wlkabs_wlk_vn1/(nwalk_passes-1)
      e_num_gen_var=e_num_gen_vn1/(passes-1); e_den_gen_var=e_den_gen_vn1/(passes-1); e_num_den_gen_var=e_num_den_gen_vn1/(passes-1)
      e_num_wlkabs_gen_var=e_num_wlkabs_gen_vn1/(passes-1); e_den_wlkabs_gen_var=e_den_wlkabs_gen_vn1/(passes-1); e_num_den_wlkabs_gen_var=e_num_den_wlkabs_gen_vn1/(passes-1)
      e_num_genabs_gen_var=e_num_genabs_gen_vn1/(passes-1); e_den_genabs_gen_var=e_den_genabs_gen_vn1/(passes-1); e_num_den_genabs_gen_var=e_num_den_genabs_gen_vn1/(passes-1)
      e_num_blk_var=e_num_blk_vn1/(iblk-1); e_den_blk_var=e_den_blk_vn1/(iblk-1); e_num_den_blk_var=e_num_den_blk_vn1/(iblk-1)
      e_num_wlkabs_blk_var=e_num_wlkabs_blk_vn1/(iblk-1); e_den_wlkabs_blk_var=e_den_wlkabs_blk_vn1/(iblk-1); e_num_den_wlkabs_blk_var=e_num_den_wlkabs_blk_vn1/(iblk-1)
      e_num_genabs_blk_var=e_num_genabs_blk_vn1/(iblk-1); e_den_genabs_blk_var=e_den_genabs_blk_vn1/(iblk-1); e_num_den_genabs_blk_var=e_num_den_genabs_blk_vn1/(iblk-1)
      e_num_blkabs_blk_var=e_num_blkabs_blk_vn1/(iblk-1); e_den_blkabs_blk_var=e_den_blkabs_blk_vn1/(iblk-1); e_num_den_blkabs_blk_var=e_num_den_blkabs_blk_vn1/(iblk-1)

      e_wlk_ave=e_num_wlk_ave/e_den_wlk_ave
      e_wlkabs_wlk_ave=e_num_wlkabs_wlk_ave/e_den_wlkabs_wlk_ave
      e_gen_ave=e_num_gen_ave/e_den_gen_ave
      e_wlkabs_gen_ave=e_num_wlkabs_gen_ave/e_den_wlkabs_gen_ave
      e_genabs_gen_ave=e_num_genabs_gen_ave/e_den_genabs_gen_ave
      e_blk_ave=e_num_blk_ave/e_den_blk_ave
      e_wlkabs_blk_ave=e_num_wlkabs_blk_ave/e_den_wlkabs_blk_ave
      e_genabs_blk_ave=e_num_genabs_blk_ave/e_den_genabs_blk_ave
      e_blkabs_blk_ave=e_num_blkabs_blk_ave/e_den_blkabs_blk_ave

      e_wlk_err=sqrt(e_wlk_ave**2*(e_num_wlk_var/e_num_wlk_ave**2+e_den_wlk_var/e_den_wlk_ave**2-2*e_num_den_wlk_var/(e_num_wlk_ave*e_den_wlk_ave))/(nwalk_passes-1))
      e_wlkabs_wlk_err=sqrt(e_wlkabs_wlk_ave**2*(e_num_wlkabs_wlk_var/e_num_wlkabs_wlk_ave**2+e_den_wlkabs_wlk_var/e_den_wlkabs_wlk_ave**2-2*e_num_den_wlkabs_wlk_var/(e_num_wlkabs_wlk_ave*e_den_wlkabs_wlk_ave))/(nwalk_passes-1))
      e_gen_err=sqrt(e_gen_ave**2*(e_num_gen_var/e_num_gen_ave**2+e_den_gen_var/e_den_gen_ave**2-2*e_num_den_gen_var/(e_num_gen_ave*e_den_gen_ave))/(passes-1))
      e_wlkabs_gen_err=sqrt(e_wlkabs_gen_ave**2*(e_num_wlkabs_gen_var/e_num_wlkabs_gen_ave**2+e_den_wlkabs_gen_var/e_den_wlkabs_gen_ave**2-2*e_num_den_wlkabs_gen_var/(e_num_wlkabs_gen_ave*e_den_wlkabs_gen_ave))/(passes-1))
      e_genabs_gen_err=sqrt(e_genabs_gen_ave**2*(e_num_genabs_gen_var/e_num_genabs_gen_ave**2+e_den_genabs_gen_var/e_den_genabs_gen_ave**2-2*e_num_den_genabs_gen_var/(e_num_genabs_gen_ave*e_den_genabs_gen_ave))/(passes-1))
      e_blk_err=sqrt(e_blk_ave**2*(e_num_blk_var/e_num_blk_ave**2+e_den_blk_var/e_den_blk_ave**2-2*e_num_den_blk_var/(e_num_blk_ave*e_den_blk_ave))/(iblk-1))
      e_wlkabs_blk_err=sqrt(e_wlkabs_blk_ave**2*(e_num_wlkabs_blk_var/e_num_wlkabs_blk_ave**2+e_den_wlkabs_blk_var/e_den_wlkabs_blk_ave**2-2*e_num_den_wlkabs_blk_var/(e_num_wlkabs_blk_ave*e_den_wlkabs_blk_ave))/(iblk-1))
      e_genabs_blk_err=sqrt(e_genabs_blk_ave**2*(e_num_genabs_blk_var/e_num_genabs_blk_ave**2+e_den_genabs_blk_var/e_den_genabs_blk_ave**2-2*e_num_den_genabs_blk_var/(e_num_genabs_blk_ave*e_den_genabs_blk_ave))/(iblk-1))
      e_blkabs_blk_err=sqrt(e_blkabs_blk_ave**2*(e_num_blkabs_blk_var/e_num_blkabs_blk_ave**2+e_den_blkabs_blk_var/e_den_blkabs_blk_ave**2-2*e_num_den_blkabs_blk_var/(e_num_blkabs_blk_ave*e_den_blkabs_blk_ave))/(iblk-1))

      t_corr=(e_blk_err(1)/e_gen_err(1))**2

!! The best energy is the one with the largest undoing of the population control for which the slope is getting monotonically less negative.
! We choose the best energy to be the one with the largest undoing of the population control for which the energy plus 2 times the error went down monotonically.
! First assume that the last is best and then if it starts going up set it to previous point and exit.
      e_blkabs_blk_ave_best=e_blkabs_blk_ave(n_pop_cntrl_corr)
      e_blkabs_blk_err_best=e_blkabs_blk_err(n_pop_cntrl_corr)
      i_pop_cntrl_corr_best=n_pop_cntrl_corr
      do i=2,n_pop_cntrl_corr
        e_change=e_blkabs_blk_ave(i)-e_blkabs_blk_ave(i-1)
        e_slope=e_change/(1/log(reweight_factor_pow(i-1))-1/log(reweight_factor_pow(i)))
        !if(e_change.gt.0_rk .or. (e_slope/e_slope_prev.gt.1.2_rk .and. i.ge.3)) then
        if(e_blkabs_blk_ave(i)+2*e_blkabs_blk_err(i).gt.e_blkabs_blk_ave(i-1)+2*e_blkabs_blk_err(i-1)) then
          e_blkabs_blk_ave_best=e_blkabs_blk_ave(i-1)
          e_blkabs_blk_err_best=e_blkabs_blk_err(i-1)
          i_pop_cntrl_corr_best=i-1
          exit
        endif
        e_slope_prev=e_slope
      enddo

    else
      t_corr=0
    endif ! iblk>=2

    write(6,'(i6, 2(f14.8,''(''i8,'')''),2f8.2)') iblk, e_blk_ave(1), nint(100000000*e_blk_err(1)), e_blk_ave(i_pop_cntrl_corr_best), nint(100000000*e_blk_err(i_pop_cntrl_corr_best)), t_corr_nonint, t_corr

    if(iblk.eq.nblk .or. (iblk.ge.2 .and. ipr.ge.0)) then
      write(6,'(/,''For undoing pop control, i_pop_cntrl_corr_best, power, number of generations='',i5,f8.4,f8.1)') &
 &    i_pop_cntrl_corr_best, reweight_factor_pow(i_pop_cntrl_corr_best), -1/log(reweight_factor_pow(i_pop_cntrl_corr_best))
      write(6,'(''Pop_control uncorrected, corrected energy='',2(f13.8,''(''i8,'')''),'' t_corr_nonint, t_corr, nstep='',2f8.2,i8,/)') &
 &    e_blkabs_blk_ave(1), nint(100000000*e_blkabs_blk_err(1)), e_blkabs_blk_ave_best, nint(100000000*e_blkabs_blk_err_best),&
 &    t_corr_nonint, t_corr, nstep

      write(6,'(''e_wlk_ave(e_wlk_err)=              '',2(f13.8,''(''i8,'')''))') e_wlk_ave   ,nint(100000000*e_wlk_err)
      write(6,'(''e_gen_ave(e_gen_err)=              '',2(f13.8,''(''i8,'')''))') (e_gen_ave(i),nint(100000000*e_gen_err(i)),i=1,i_pop_cntrl_corr_best,i_pop_cntrl_corr_best-1)
      write(6,'(''e_blk_ave(e_blk_err)=              '',2(f13.8,''(''i8,'')''))') (e_blk_ave(i),nint(100000000*e_blk_err(i)),i=1,i_pop_cntrl_corr_best,i_pop_cntrl_corr_best-1)
      write(6,'(''e_wlkabs_wlk_ave(e_wlkabs_wlk_err)='',2(f13.8,''(''i8,'')''))') e_wlkabs_wlk_ave,nint(100000000*e_wlkabs_wlk_err)
      write(6,'(''e_wlkabs_gen_ave(e_wlkabs_gen_err)='',2(f13.8,''(''i8,'')''))') (e_wlkabs_gen_ave(i),nint(100000000*e_wlkabs_gen_err(i)),i=1,i_pop_cntrl_corr_best,i_pop_cntrl_corr_best-1)
      write(6,'(''e_wlkabs_blk_ave(e_wlkabs_blk_err)='',2(f13.8,''(''i8,'')''))') (e_wlkabs_blk_ave(i),nint(100000000*e_wlkabs_blk_err(i)),i=1,i_pop_cntrl_corr_best,i_pop_cntrl_corr_best-1)
      write(6,'(''e_genabs_gen_ave(e_genabs_gen_err)='',2(f13.8,''(''i8,'')''))') (e_genabs_gen_ave(i),nint(100000000*e_genabs_gen_err(i)),i=1,i_pop_cntrl_corr_best,i_pop_cntrl_corr_best-1)
      write(6,'(''e_genabs_blk_ave(e_genabs_blk_err)='',2(f13.8,''(''i8,'')''))') (e_genabs_blk_ave(i),nint(100000000*e_genabs_blk_err(i)),i=1,i_pop_cntrl_corr_best,i_pop_cntrl_corr_best-1)
      write(6,'(''e_blkabs_blk_ave(e_blkabs_blk_err)='',2(f13.8,''(''i8,'')''))') (e_blkabs_blk_ave(i),nint(100000000*e_blkabs_blk_err(i)),i=1,i_pop_cntrl_corr_best,i_pop_cntrl_corr_best-1)
      write(6,'(''t_corr='',9f8.2)') (e_blk_err(1)/e_gen_err(1))**2, (e_genabs_blk_err(1)/e_genabs_gen_err(1))**2
    endif

    if(ipr.ge.1) then
      write(6,'(''e_wlk_ave(e_wlk_err)=              '',100(f13.8,''(''i8,'')''))') e_wlk_ave   ,nint(100000000*e_wlk_err)
      write(6,'(''e_gen_ave(e_gen_err)=              '',100(f13.8,''(''i8,'')''))') (e_gen_ave(i),nint(100000000*e_gen_err(i)),i=1,n_pop_cntrl_corr)
      write(6,'(''e_blk_ave(e_blk_err)=              '',100(f13.8,''(''i8,'')''))') (e_blk_ave(i),nint(100000000*e_blk_err(i)),i=1,n_pop_cntrl_corr)
      write(6,'(''e_wlkabs_wlk_ave(e_wlkabs_wlk_err)='',100(f13.8,''(''i8,'')''))') e_wlkabs_wlk_ave,nint(100000000*e_wlkabs_wlk_err)
      write(6,'(''e_wlkabs_gen_ave(e_wlkabs_gen_err)='',100(f13.8,''(''i8,'')''))') (e_wlkabs_gen_ave(i),nint(100000000*e_wlkabs_gen_err(i)),i=1,n_pop_cntrl_corr)
      write(6,'(''e_wlkabs_blk_ave(e_wlkabs_blk_err)='',100(f13.8,''(''i8,'')''))') (e_wlkabs_blk_ave(i),nint(100000000*e_wlkabs_blk_err(i)),i=1,n_pop_cntrl_corr)
      write(6,'(''e_genabs_gen_ave(e_genabs_gen_err)='',100(f13.8,''(''i8,'')''))') (e_genabs_gen_ave(i),nint(100000000*e_genabs_gen_err(i)),i=1,n_pop_cntrl_corr)
      write(6,'(''e_genabs_blk_ave(e_genabs_blk_err)='',100(f13.8,''(''i8,'')''))') (e_genabs_blk_ave(i),nint(100000000*e_genabs_blk_err(i)),i=1,n_pop_cntrl_corr)
      write(6,'(''e_blkabs_blk_ave(e_blkabs_blk_err)='',100(f13.8,''(''i8,'')''))') (e_blkabs_blk_ave(i),nint(100000000*e_blkabs_blk_err(i)),i=1,n_pop_cntrl_corr)
      write(6,'(''t_corr='',9f8.2)') (e_blk_err(1)/e_gen_err(1))**2, (e_genabs_blk_err(1)/e_genabs_gen_err(1))**2
    endif

  enddo ! iblk

  write(6,'(/,''At end of run, reweight_factor_prod='',1000es9.2)') (reweight_factor_prod(i),i=1,n_pop_cntrl_corr)

  write(6,'(/,'' i    e_corrected(  err   )    difference    slope'')')
  do i=1,n_pop_cntrl_corr
    if(i.eq.1) then
      write(6,'(i3,f14.8,''(''i8,'')'',2f14.8)') i, e_blkabs_blk_ave(i), nint(100000000*e_blkabs_blk_err(i))
    else
      write(6,'(i3,f14.8,''(''i8,'')'',2f14.8)') i, e_blkabs_blk_ave(i), nint(100000000*e_blkabs_blk_err(i)), &
 &    e_blkabs_blk_ave(i)-e_blkabs_blk_ave(i-1), (e_blkabs_blk_ave(i)-e_blkabs_blk_ave(i-1))/(1/log(reweight_factor_pow(i-1))-1/log(reweight_factor_pow(i)))
    endif
  enddo

  call my_second(2,'run')

end subroutine walk
!---------------------------------------------------------------------------------------------------------------------

  subroutine split_join(walk_wt,e_num_w,e_den_w,nwalk,MWALK)
! ==============================================================================
! Description   : Do branching using split_join algorithm
!               : In this version walker weights must be >=0.
! Author        : Cyrus Umrigar
! ------------------------------------------------------------------------------
  use types, only: i1b, ik, rk
  implicit none
  integer, intent(in) :: MWALK
  integer, intent(inout) :: nwalk
  real(rk), intent(inout) :: walk_wt(MWALK), e_num_w(MWALK), e_den_w(MWALK)
! integer iwalk, iwalk2, nwalk2, nwalk_after_join, nwalk_max_dupl, j, iunder, ipair, nbrnch
  integer iwalk, iwalk2, nwalk2, j, iunder, ipair, nbrnch
! real(rk) rannyu, w_sum, w_sum2, wtot, w_split, w_sav(MWALK)
  real(rk) rannyu, w_sum, w_sum2, wtot, w_split
  integer iwundr(nwalk)

! if(ipr.ge.2) then
!   write(6,'(''nwalk='',9i5)') nwalk
!   write(6,'(''walk_wt='',100f6.2)') (walk_wt(iwalk),iwalk=1,nwalk)
! endif

! Identify the walkers that are to be killed because their walk_wt. is 0, or that are
! to be merged because their wts. are less than 1/2.  For each pair whose wts. are < 1/2
! give the one that is to be kept the additional walk_wt of the other and put the other one on a stack.
  iunder=0
  ipair=0
  w_sum=0._rk
  !write(6,'(''before join walk_wt='',1000f6.2)') (walk_wt(iwalk),iwalk=1,nwalk)
  do iwalk=1,nwalk
    w_sum=w_sum+walk_wt(iwalk)
    if(walk_wt(iwalk).lt.0.5_rk) then
      if(walk_wt(iwalk).eq.0._rk) then
        nbrnch=nbrnch+1
        iunder=iunder+1
        iwundr(iunder)=iwalk
       else
        if(ipair.eq.0) then
          ipair=1
          iwalk2=iwalk
         else
          nbrnch=nbrnch+1
          ipair=0
          iunder=iunder+1
          wtot=walk_wt(iwalk)+walk_wt(iwalk2)
          if(rannyu(0).gt.(walk_wt(iwalk)/wtot)) then
            walk_wt(iwalk2)=wtot
            iwundr(iunder)=iwalk
           else
            walk_wt(iwalk)=wtot
            iwundr(iunder)=iwalk2
          endif
        endif
      endif
    endif
  enddo

! Figure out what weight walkers can be split without exceeding MWALK
! nwalk_after_join=nwalk-iunder
! nwalk_max_dupl=MWALK-nwalk_after_join
! do iwalk=1,nwalk
!   w_sav(iwalk)=walk_wt(iwalk)
! enddo
! call shell(w_sav,nwalk)
! w_split=w_sav(max(1,nwalk-nwalk_max_dupl))

! For now just split walkers with weight >=2
  w_split=2._rk

! Split the walkers whose walk_wt is >max(2._rk,w_split).  If there are walkers that were eliminated, so that iunder>0
! then put the new walker in that location.  Otherwise put it at the end.
  nwalk2=nwalk
  do iwalk=1,nwalk
    if(walk_wt(iwalk).gt.max(2._rk,w_split)) then
      nbrnch=nbrnch+1
      if(iunder.gt.0) then
        iwalk2=iwundr(iunder)
        iunder=iunder-1
       else
        nwalk2=nwalk2+1
        iwalk2=nwalk2
        if(nwalk2.gt.MWALK) then
          write(6,'(''iwalk,nwalk,nwalk2,MWALK='',9i9)') iwalk,nwalk,nwalk2,MWALK
          write(6,'(''walk_wt='',100f6.2)') walk_wt(1:nwalk)
          stop 'MWALK exceeded in splitj_movall'
        endif
      endif
      walk_wt(iwalk)=walk_wt(iwalk)*0.5_rk
      walk_wt(iwalk2)=walk_wt(iwalk)
      e_num_w(iwalk2)=e_num_w(iwalk)
      e_den_w(iwalk2)=e_den_w(iwalk)
    endif
  enddo

! If more walkers were eliminated than the number duplicated then consolidate
! the remaining walkers so that they are they occupy the first positions.
  do j=iunder,1,-1
    iwalk2=iwundr(j)
    iwalk=nwalk2
    nwalk2=nwalk2-1
    walk_wt(iwalk2)=walk_wt(iwalk)
    e_num_w(iwalk2)=e_num_w(iwalk)
    e_den_w(iwalk2)=e_den_w(iwalk)
  enddo
  nwalk=nwalk2

  w_sum2=sum(walk_wt(1:nwalk))
  if(dabs(w_sum-w_sum2).gt.1.d-12*dabs(w_sum)) write(11,'(2f14.6)') w_sum,w_sum2
  !write(6,'(''after split walk_wt='',1000f6.2)') (walk_wt(iwalk),iwalk=1,nwalk)

  return
  end subroutine split_join
!---------------------------------------------------------------------------------------------------------------------

  subroutine split_join_signed(walk_wt,e_num_w,e_den_w,nwalk,MWALK)
! ==============================================================================
! Description   : Do branching using split_join algorithm
!               : In this version walker weights can be of either sign.
! Author        : Cyrus Umrigar
! ------------------------------------------------------------------------------
  use types, only: i1b, ik, rk
  implicit none
  integer, intent(in) :: MWALK
  integer, intent(inout) :: nwalk
  real(rk), intent(inout) :: walk_wt(MWALK), e_num_w(MWALK), e_den_w(MWALK)
! integer iwalk, iwalk2, nwalk2, nwalk_after_join, nwalk_max_dupl, j, iunder, ipair, nbrnch
  integer iwalk, iwalk2, nwalk2, j, iunder, ipair, nbrnch
! real(rk) rannyu, w_sum, w_sum2, wtot, w_split, w_sav(MWALK)
! real(rk) rannyu, w_sum, w_sum2, w_abs_sum, w_abs_sum2, w_abs_tot, w_split
  real(rk) rannyu, w_abs_sum, w_abs_sum2, w_abs_tot, w_split
  integer iwundr(nwalk)

! if(ipr.ge.2) then
!   write(6,'(''nwalk='',9i5)') nwalk
!   write(6,'(''i walk_wt='',100f6.2)') (walk_wt(iwalk),iwalk=1,nwalk)
!   write(6,'(''i e_num_w='',100f6.2)') (e_num_w(iwalk),iwalk=1,nwalk)
! endif

! Identify the walkers that are to be killed because their walk_wt. is 0, or that are
! to be merged because their wts. are less than 1/2.  For each pair whose wts. are < 1/2
! give the one that is to be kept the additional walk_wt of the other and put the other one on a stack.
  iunder=0
  ipair=0
! w_sum=0._rk
  w_abs_sum=0._rk
  !write(6,'(''before join walk_wt='',1000f6.2)') (walk_wt(iwalk),iwalk=1,nwalk)
  do iwalk=1,nwalk
!   w_sum=w_sum+walk_wt(iwalk)
    w_abs_sum=w_abs_sum+abs(walk_wt(iwalk))
    if(abs(walk_wt(iwalk)).lt.0.5_rk) then
      if(abs(walk_wt(iwalk)).eq.0._rk) then
        nbrnch=nbrnch+1
        iunder=iunder+1
        iwundr(iunder)=iwalk
       else
        if(ipair.eq.0) then
          ipair=1
          iwalk2=iwalk
         else
          nbrnch=nbrnch+1
          ipair=0
          iunder=iunder+1
          w_abs_tot=abs(walk_wt(iwalk))+abs(walk_wt(iwalk2))
          if(rannyu(0).gt.(abs(walk_wt(iwalk))/w_abs_tot)) then
            walk_wt(iwalk2)=w_abs_tot*sign(1._rk,walk_wt(iwalk2))
            iwundr(iunder)=iwalk
           else
            walk_wt(iwalk)=w_abs_tot*sign(1._rk,walk_wt(iwalk))
            iwundr(iunder)=iwalk2
          endif
        endif
      endif
    endif
  enddo

! Figure out what weight walkers can be split without exceeding MWALK
! nwalk_after_join=nwalk-iunder
! nwalk_max_dupl=MWALK-nwalk_after_join
! do iwalk=1,nwalk
!   w_sav(iwalk)=walk_wt(iwalk)
! enddo
! call shell(w_sav,nwalk)
! w_split=w_sav(max(1,nwalk-nwalk_max_dupl))

! For now just split walkers with weight <=2
  w_split=2._rk

! Split the walkers whose walk_wt is >max(1._rk,w_split).  If there are walkers that were eliminated, so that iunder>0
! then put the new walker in that location.  Otherwise put it at the end.
  nwalk2=nwalk
  do iwalk=1,nwalk
    if(walk_wt(iwalk).gt.max(2._rk,w_split)) then
      nbrnch=nbrnch+1
      if(iunder.gt.0) then
        iwalk2=iwundr(iunder)
        iunder=iunder-1
       else
        nwalk2=nwalk2+1
        iwalk2=nwalk2
        if(nwalk2.gt.MWALK) then
          write(6,'(''iwalk,nwalk,nwalk2,MWALK='',9i9)') iwalk,nwalk,nwalk2,MWALK
          write(6,'(''walk_wt='',100f6.2)') walk_wt(1:nwalk)
          stop 'MWALK exceeded in splitj_movall'
        endif
      endif
      walk_wt(iwalk)=walk_wt(iwalk)*0.5_rk
      walk_wt(iwalk2)=walk_wt(iwalk)
      e_num_w(iwalk2)=e_num_w(iwalk)
      e_den_w(iwalk2)=e_den_w(iwalk)
    endif
  enddo

! If more walkers were eliminated than the number duplicated then consolidate
! the remaining walkers so that they are they occupy the first positions.
  do j=iunder,1,-1
    iwalk2=iwundr(j)
    iwalk=nwalk2
    nwalk2=nwalk2-1
    walk_wt(iwalk2)=walk_wt(iwalk)
    e_num_w(iwalk2)=e_num_w(iwalk)
    e_den_w(iwalk2)=e_den_w(iwalk)
  enddo
  nwalk=nwalk2

! w_sum2=sum(walk_wt(1:nwalk))
  w_abs_sum2=sum(abs(walk_wt(1:nwalk)))
  if(dabs(w_abs_sum-w_abs_sum2).gt.1.d-12*dabs(w_abs_sum)) write(6,'(''Warning: w_abs_sum,w_abs_sum2'',2f14.6)') w_abs_sum,w_abs_sum2
  !write(6,'(''after split walk_wt='',1000f6.2)') (walk_wt(iwalk),iwalk=1,nwalk)
! write(6,'(''o walk_wt='',100f6.2)') (walk_wt(iwalk),iwalk=1,nwalk)
! write(6,'(''o e_num_w='',100f6.2)') (e_num_w(iwalk),iwalk=1,nwalk)
! write(6,*)

  return
  end subroutine split_join_signed
!---------------------------------------------------------------------------------------------------------------------

  subroutine comb_signed(w,e_num_w,e_den_w,nwalk)
! ==============================================================================
! Description   : Do branching using comb algorithm
!               : In this version walker weights can be of either sign.
! Author        : Cyrus Umrigar
! ------------------------------------------------------------------------------
  use types, only: i1b, ik, rk
  implicit none
  integer, intent(in) :: nwalk
  real(rk), intent(inout) :: w(nwalk), e_num_w(nwalk), e_den_w(nwalk)
  integer iwalk, iwalk_old, iwalk_old_begin
  real(rk) comb_tine, random, rannyu
  real(rk) w_abs_av, w_partial_sum(nwalk), w_tmp(nwalk), e_num_tmp(nwalk), e_den_tmp(nwalk)

! write(6,'(''i w=      '',100f8.4)') w(1:nwalk)
! write(6,'(''i e_num_w='',100f8.4)') e_num_w(1:nwalk)
! write(6,'(''i e_den_w='',100f8.4)') e_den_w(1:nwalk)

! Set cumulative probabilities
  do iwalk=1,nwalk
    if(iwalk.eq.1) then
      w_partial_sum(iwalk)=abs(w(iwalk))
     else
      w_partial_sum(iwalk)=w_partial_sum(iwalk-1)+abs(w(iwalk))
    endif
  enddo
  w_abs_av=w_partial_sum(nwalk)/nwalk

  if(ipr.ge.1) then
    write(6,'(/,''wo           ='',1000f10.5)') w(1:nwalk)
    write(6,'(''w_partial_sum='',1000f10.5)') w_partial_sum(1:nwalk)
    write(6,'(''e_num_old     ='',1000f10.5)') e_num_w(1:nwalk)
    write(6,'(''e_den_old     ='',1000f10.5)') e_num_w(1:nwalk)
  endif

  random=rannyu(0)
  random=mod(random,1._rk/nwalk) ! This is a random number between 0 and 1/nwalk
  iwalk_old_begin=1
  do iwalk=1,nwalk
    comb_tine=random+dfloat(iwalk-1)/nwalk
    do iwalk_old=iwalk_old_begin,nwalk
       if(w_partial_sum(iwalk_old).gt.comb_tine*w_partial_sum(nwalk)) then
         iwalk_old_begin=iwalk_old
         exit
       endif
    enddo
    e_num_tmp(iwalk)=e_num_w(iwalk_old_begin)
    e_den_tmp(iwalk)=e_den_w(iwalk_old_begin)
    w_tmp(iwalk)=w_abs_av*sign(1._rk,w(iwalk_old_begin))
    if(iwalk_old_begin.gt.nwalk) stop 'iwalk_old_begin > nwalk'
  enddo
! w=w_partial_sum(nwalk)/nwalk
  w=w_tmp
  e_num_w=e_num_tmp
  e_den_w=e_den_tmp

  if(ipr.ge.1) then
    write(6,'(/,''wn           ='',1000f10.5)') w(1:nwalk)
    write(6,'(''e_num_new    ='',1000f10.5)') e_num_w(1:nwalk)
    write(6,'(''e_den_new    ='',1000f10.5)') e_den_w(1:nwalk)
  endif

! write(6,'(''o w=      '',100f8.4)') w(1:nwalk)
! write(6,'(''o e_num_w='',100f8.4)') e_num_w(1:nwalk)
! write(6,'(''o e_den_w='',100f8.4)') e_den_w(1:nwalk)
! write(6,*)

  call flush(6)

  end subroutine comb_signed

end program template2
!---------------------------------------------------------------------------------------------------------------------
  subroutine stats(n_pop_cntrl_corr, ndata, e_num, e_den, &
 &    e_num_ave, e_num_vn1, e_den_ave, e_den_vn1, e_num_den_vn1)
! Uses Welford's algorithm to calculate the average and the variance times (ndata-1) for the numerator and denominator and their covariance times (ndata-1)
  use types, only: i8b, rk
  implicit none
  integer, intent(in) :: n_pop_cntrl_corr
  integer(i8b), intent(in) :: ndata
  real(rk), intent(in) :: e_num(n_pop_cntrl_corr), e_den(n_pop_cntrl_corr)
  real(rk), intent(inout) :: e_num_ave(n_pop_cntrl_corr), e_num_vn1(n_pop_cntrl_corr), e_den_ave(n_pop_cntrl_corr), e_den_vn1(n_pop_cntrl_corr), e_num_den_vn1(n_pop_cntrl_corr)
  real(rk) e_num_del, e_den_del
  integer i

  do i=1,n_pop_cntrl_corr
    e_num_del=e_num(i)-e_num_ave(i)
    e_num_ave(i)=e_num_ave(i)+e_num_del/ndata
    e_num_vn1(i)=e_num_vn1(i)+(e_num(i)-e_num_ave(i))*e_num_del

    e_den_del=e_den(i)-e_den_ave(i)
    e_den_ave(i)=e_den_ave(i)+e_den_del/ndata
    e_den_vn1(i)=e_den_vn1(i)+(e_den(i)-e_den_ave(i))*e_den_del

    e_num_den_vn1(i)=e_num_den_vn1(i)+(e_num(i)-e_num_ave(i))*e_den_del
  enddo

  return
  end subroutine stats
