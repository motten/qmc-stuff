program template

  integer ipr

  call walk

!------------------------------------------------------------------------------------------------------------

  contains

  subroutine walk
! Uses Welford's algorithm for calculating standard deviations
  use types, only: i1b, ik, rk
  implicit none

  integer, dimension(4) :: irand_seed
  integer, parameter :: MWALK=1000000, n_pop_control_corrected=21
  real(rk) rannyu, noise, fraction_negative_wt, e_num_prev, e_den_prev, p, q, e_trial, e_loc, e_est, tau, reweight_factor_inv, reweight_factor_prod(n_pop_control_corrected), reweight_factor_pow(n_pop_control_corrected), pop_control_pow(n_pop_control_corrected), &
  w_abs_gen, w_abs_gen_imp, w_perm_initiator_gen, &
  w_blk, w_abs_blk, w_abs_blk_imp, w_perm_initiator_blk, &
  w_abs_run, ran
  integer(ik)  passes, nwalk_passes, nwalk_blk, nwalk_run
  integer :: i, i_pop_control_corrected_best=0

  integer iwalk, istep, iblk, iblkk, nwalk_target, nwalk, nstep, nblk, nblk_eq, ntimes_nblk_eq
  real(rk) &
  e_gen, e_blk, e_blk_ave, e_blk_err, &
  e_abs_ave, e_genabs_ave, e_blkabs_ave, &
  e_abs_err, e_genabs_err, e_blkabs_err, &
  e_num, e_num_abs, e_num_abs_del, e_num_abs_ave, e_num_abs_vn1, e_num_abs_var, &
  e_num_gen, e_num_genabs, e_num_genabs_del, e_num_genabs_ave, e_num_genabs_vn1, e_num_genabs_var, &
  e_num_blk, e_num_blkabs, e_num_blkabs_del, e_num_blkabs_ave, e_num_blkabs_vn1, e_num_blkabs_var, &
  e_num_blk_del, e_num_blk_ave, e_num_blk_vn1, e_num_blk_var, &
  e_num_cum, &
  e_den, e_den_abs, e_den_abs_del, e_den_abs_ave, e_den_abs_vn1, e_den_abs_var, &
  e_den_gen, e_den_genabs, e_den_genabs_del, e_den_genabs_ave, e_den_genabs_vn1, e_den_genabs_var, &
  e_den_blk, e_den_blkabs, e_den_blkabs_del, e_den_blkabs_ave, e_den_blkabs_vn1, e_den_blkabs_var, &
  e_den_blk_del, e_den_blk_ave, e_den_blk_vn1, e_den_blk_var, &
  e_den_cum, &
  e_num_abs_e_den_abs_vn1, e_num_genabs_e_den_genabs_vn1, e_num_blkabs_e_den_blkabs_vn1, e_num_blk_e_den_blk_vn1, &
  e_num_abs_e_den_abs_var, e_num_genabs_e_den_genabs_var, e_num_blkabs_e_den_blkabs_var, e_num_blk_e_den_blk_var, &
  e_gen_del, e_gen_ave, e_gen_vn1, &
  e_genp, e_genp_ave, e_genpp_del,e_genpp_ave, e_genp_e_genpp_vn1, &
  tau_corr_nonint, t_corr_nonint, t_corr, &
  walk_wt(MWALK), e_num_w(MWALK), e_den_w(MWALK), &
  e_num_true, e_den_true, population_control_exponent, t_corr_est, &
  e_num_corrected_blk(n_pop_control_corrected), e_num_corrected_blkabs(n_pop_control_corrected), e_num_corrected_blkabs_del(n_pop_control_corrected), e_num_corrected_blkabs_ave(n_pop_control_corrected), e_num_corrected_blkabs_vn1(n_pop_control_corrected), e_num_corrected_blkabs_var(n_pop_control_corrected), &
  e_den_corrected_blk(n_pop_control_corrected), e_den_corrected_blkabs(n_pop_control_corrected), e_den_corrected_blkabs_del(n_pop_control_corrected), e_den_corrected_blkabs_ave(n_pop_control_corrected), e_den_corrected_blkabs_vn1(n_pop_control_corrected), e_den_corrected_blkabs_var(n_pop_control_corrected), &
  e_num_corrected_blkabs_e_den_corrected_blkabs_vn1(n_pop_control_corrected), e_num_corrected_blkabs_e_den_corrected_blkabs_var(n_pop_control_corrected), &
  e_corrected_blkabs_ave(n_pop_control_corrected), e_corrected_blkabs_err(n_pop_control_corrected), &
  e_corrected_blkabs_ave_best, e_corrected_blkabs_err_best, e_change, e_slope, e_slope_prev
  
  real(rk) ratio(4)
  character*16 method

  ipr=0
  e_num_prev=-2; e_den_prev=1; e_num_true=-2; e_den_true=1; e_trial=-2

  write(6,'(''template'')')
  read(5,'(4i4,x,4i4)') irand_seed
  write(6,'(/,''random number seeds'',t25,4i4,x,4i4)') irand_seed
  call setrn(irand_seed)
  read(5,*) nstep, nblk, nblk_eq
  read(5,*) nwalk_target
  read(5,*) tau, t_corr_est, population_control_exponent
  read(5,*) noise, fraction_negative_wt
  read(5,*) method
  write(6,'(''nstep, nblk, nblk_eq='',9i8)') nstep, nblk, nblk_eq
  write(6,'(''nwalk_target='',9i8)') nwalk_target
  write(6,'(''tau, t_corr_est, population_control_exponent='',9f10.5)') tau, t_corr_est, population_control_exponent
  write(6,'(''noise, fraction_negative_wt='',9f10.5)') noise, fraction_negative_wt
  write(6,'(''If noise>2, e_den will be of both signs; if noise>4, e_den will be of both signs'',/)')
  write(6,'(''method='',a)') method
  p=exp(-tau/t_corr_est)
  q=1-p
  reweight_factor_inv=1
  reweight_factor_pow(1)=0
  do i=2,n_pop_control_corrected
    !reweight_factor_pow(i)=1-exp(-0.5*i*tau/t_corr_est)
    !reweight_factor_pow(i)=1-tau/(tau+i*t_corr_est)
    !if(i.ge.2) reweight_factor_pow(i)=0.2+0.8*reweight_factor_pow(i-1)
    !if(i.ge.2) reweight_factor_pow(i)=0.25+0.75*reweight_factor_pow(i-1)
    !if(i.ge.2) reweight_factor_pow(i)=(0.25**(1/(1.4**(i-2)*t_corr_est)))
    !if(i.ge.2) reweight_factor_pow(i)=real(i-1,rk)/(n_pop_control_corrected-1)
    !if(i.ge.2) reweight_factor_pow(i)=0.4*p+(1-0.4*p)*reweight_factor_pow(i-1)
    reweight_factor_pow(i)=exp(-1/sqrt(2._rk)**(i-2))
    reweight_factor_prod(i)=1
  enddo
  write(6,'(/,''reweight_factor_pow='',100f8.3)') (reweight_factor_pow(i),i=1,n_pop_control_corrected)
  write(6,'(''Number of generations='',100f8.1,/)') (-1/log(reweight_factor_pow(i)),i=1,n_pop_control_corrected)

  if(nwalk.gt.MWALK) stop 'nwalk>MWALK'
  nwalk=nwalk_target

  e_est=e_num_true/e_den_true
  w_abs_gen=nwalk

  do iwalk=1,nwalk
    e_num_w(iwalk)=e_num_true+(rannyu(0)-0.5_rk)*noise
    e_den_w(iwalk)=e_den_true+(rannyu(0)-0.5_rk)*noise
    walk_wt(iwalk)=1
  enddo
  if(ipr.ge.1) then
    write(6,'(100f10.5)') e_num_w(1:nwalk)
    write(6,'(100f10.5)') e_den_w(1:nwalk)
  endif

  call my_second(1,'run')

! Walk the walk
  ntimes_nblk_eq=2
! do while (iblkk <= ntimes_nblk_eq*nblk_eq+nblk)
  do iblkk=1,ntimes_nblk_eq*nblk_eq+nblk

! The number of equilibration blocks is ntimes_nblk_eq * nblk_eq.  Before each set nblk_eq equilibration blocks set estimators to 0.
!   if(iblkk.eq.1 .or. iblkk.eq.nblk_eq+1 .or. iblkk.eq.2*nblk_eq+1) then
    if(mod(iblkk,nblk_eq).eq.1 .and. iblkk.le.ntimes_nblk_eq*nblk_eq+1) then
!     if(e_den_blk_ave.ne.0) then
!       write(6,'(/,''iblkk='',i3,'' resetting e_trial from'',f10.5,'' to'',f10.5,/)') iblkk, e_trial, e_num_blk_ave/e_den_blk_ave
!       e_trial=e_num_blk_ave/e_den_blk_ave
!     endif
      nwalk_run=0; w_abs_run=0
      e_num_abs_ave=0; e_num_abs_vn1=0
      e_num_genabs_ave=0; e_num_genabs_vn1=0
      e_num_blkabs_ave=0; e_num_blkabs_vn1=0
      e_num_blk_ave=0; e_num_blk_vn1=0
      e_den_abs_ave=0; e_den_abs_vn1=0
      e_den_genabs_ave=0; e_den_genabs_vn1=0
      e_den_blkabs_ave=0; e_den_blkabs_vn1=0
      e_den_blk_ave=0; e_den_blk_vn1=0
      e_num_abs_e_den_abs_vn1=0; e_num_genabs_e_den_genabs_vn1=0; e_num_blkabs_e_den_blkabs_vn1=0 ; e_num_blk_e_den_blk_vn1=0
      e_gen_ave=0; e_gen_vn1=0
      e_genp_ave=0; e_genpp_ave=0; e_genp_e_genpp_vn1=0 
      nwalk_passes=0
      e_num_corrected_blkabs_ave(1:n_pop_control_corrected)=0
      e_num_corrected_blkabs_vn1(1:n_pop_control_corrected)=0
      e_den_corrected_blkabs_ave(1:n_pop_control_corrected)=0
      e_den_corrected_blkabs_vn1(1:n_pop_control_corrected)=0
      e_num_corrected_blkabs_e_den_corrected_blkabs_vn1(1:n_pop_control_corrected)=0
    endif

!   if(iblkk.le.nblk_eq) then
!     iblk=iblkk
!    elseif(iblkk.le.2*nblk_eq) then
!     iblk=iblkk-nblk_eq
!    else
!     iblk=iblkk-2*nblk_eq
!   endif
    if(iblkk.le.ntimes_nblk_eq*nblk_eq) then
      iblk=mod(iblkk-1,nblk_eq)+1
    else
      iblk=iblkk-ntimes_nblk_eq*nblk_eq
    endif

    nwalk_blk=0 ; w_abs_blk=0 ; w_blk=0 ; e_num_blk=0 ; e_den_blk=0
    e_num_corrected_blk(1:n_pop_control_corrected)=0
    e_den_corrected_blk(1:n_pop_control_corrected)=0

    do istep=1,nstep

!egr      e_gro_den_gen=sum(walk_wt(1:nwalk))
!egr      do i=1,n_pop_control_corrected
!egr        e_gro_den_corrected_blk(i)=e_gro_den_corrected_blk(i)+reweight_factor_prod(i)*e_gro_den_gen
!egr      enddo

!     write(6,'(''reweight_factor_inv, e_trial, e_est, w_abs_gen, population_control_exponent'',9es12.4)') reweight_factor_inv, e_trial, e_est, w_abs_gen, population_control_exponent
!     reweight_factor_inv = min(100.d0, max(1.d-2, (1+tau*(e_trial-e_est))*(dfloat(nwalk_target)/w_abs_gen)**min(1.d0,tau*population_control_exponent)))
      reweight_factor_inv = (1+tau*(e_trial-e_est))*(dfloat(nwalk_target)/abs(w_abs_gen))**min(1.d0,tau*population_control_exponent)
      if(reweight_factor_inv.lt.0) then
        write(6,'(''reweight_factor_inv, e_trial, e_est, w_abs_gen, population_control_exponent'',9es12.4)') reweight_factor_inv, e_trial, e_est, w_abs_gen, population_control_exponent
        !reweight_factor_inv = min(100.d0, max(1.d-2,reweight_factor_inv))
        reweight_factor_inv = min(10.d0, max(1.d-1,reweight_factor_inv))
        write(6,'(''e_trial, e_est, w_abs_gen'',9f10.5)') e_trial, e_est, w_abs_gen
        !stop 'reweight_factor_inv.lt.0'
      endif
      !write(6,'(''e_trial, e_est, (1+tau*(e_trial-e_est)), nwalk_target, w_abs_gen, reweight_factor_inv'', 3f10.6,i8,9f10.6)') e_trial, e_est, (1+tau*(e_trial-e_est)), nwalk_target, w_abs_gen, reweight_factor_inv
      do i=1,n_pop_control_corrected
! Warning: tmp.  Now restored
!       reweight_factor_prod(i)=(reweight_factor_prod(i)**reweight_factor_pow(i))/reweight_factor_inv
        reweight_factor_prod(i)=(reweight_factor_prod(i)/reweight_factor_inv)**reweight_factor_pow(i)
      enddo
      !write(6,'(''reweight_factor_prod='',100es11.3)') 1/reweight_factor_inv, (reweight_factor_prod(i),i=1,n_pop_control_corrected)

      !write(6,'(''e_den_blkabs_ave, e_den_blk='',9d12.4)') e_den_blkabs_ave, e_den_blk
      !if(e_den_blkabs_cum+abs(e_den_blk).ne.0) then
      if(e_den_blkabs_ave+abs(e_den_blk).ne.0) then
        e_est=((iblk-1)*e_num_blkabs_ave+e_num_blk*sign(1._rk,e_den_blk))/((iblk-1)*e_den_blkabs_ave+abs(e_den_blk))
        if(i_pop_control_corrected_best.ne.0) e_est=min(e_est, e_num_corrected_blk(i_pop_control_corrected_best)/e_den_corrected_blk(i_pop_control_corrected_best))
        !write(6,'(''set e_est='',f10.5,9i8)') e_est, iblkk, ntimes_nblk_eq,nblk_eq
        if(iblkk.le.ntimes_nblk_eq*nblk_eq) then
          e_trial=e_trial+sign(min(abs(e_est-e_trial),1._rk),e_est-e_trial) ! Warning: when we remove population control error, we should not update e_trial after first equilibration is done
        endif
        if(iblk.ge.3 .and. iblkk.le.ntimes_nblk_eq*nblk_eq .and. istep.eq.nstep) write(6,'(''At end of block'',i4,'', e_trial='',9es13.5)') iblkk, e_trial, e_num_corrected_blk(i_pop_control_corrected_best)/e_den_corrected_blk(i_pop_control_corrected_best), &
 &      e_trial - e_num_corrected_blk(i_pop_control_corrected_best)/e_den_corrected_blk(i_pop_control_corrected_best)
        if(iblkk.eq.ntimes_nblk_eq*nblk_eq .and. istep.eq.nstep) write(6,'(''At end of equil, e_trial='',9es13.5)') e_trial
        w_abs_gen=sum(abs(walk_wt(1:nwalk)))
!       w_abs_gen_imp=0
!       do iwalk=1,nwalk
!         if(imp_distance(iwalk).eq.0) w_abs_gen_imp=w_abs_gen_imp+abs(walk_wt(iwalk))
!       enddo
        if(ipr.ge.1 .or. (ipr.ge.0.and.iblkk.le.ntimes_nblk_eq*nblk_eq)) write(6,'(''iblkk, istep, w_perm_initiator_gen, nwalk, w_abs_gen, w_abs_gen_imp, e_gen, e_est, e_trial, reweight_factor_inv, t_c='',2i6,f8.1,i9,f11.1,f10.1,f10.4,2f11.5,f10.5,f8.1)') &
 &      iblkk, istep, w_perm_initiator_gen, nwalk, w_abs_gen, w_abs_gen_imp, e_num_gen/e_den_gen, e_est, e_trial, reweight_factor_inv, t_corr_nonint
        call flush(6)
      endif
!     if(iblk.ge.3 .and. istep.ge.3) write(6,'(''At end of block'',i4,'', e_trial='',9es13.5)') iblkk, e_trial, e_num_corrected_blk(i_pop_control_corrected_best)/e_den_corrected_blk(i_pop_control_corrected_best), &
!&      e_trial - e_num_corrected_blk(i_pop_control_corrected_best)/e_den_corrected_blk(i_pop_control_corrected_best)

      e_num_gen=0; e_den_gen=0

      do iwalk=1,nwalk

        e_num_w(iwalk)=p*e_num_w(iwalk)+q*(e_num_true+(rannyu(0)-0.5_rk)*noise)
        e_den_w(iwalk)=p*e_den_w(iwalk)+q*(e_den_true+(rannyu(0)-0.5_rk)*noise)
        e_loc=e_num_w(iwalk)/e_den_w(iwalk)
!       if(e_den_w(iwalk).lt.0) then
!         write(6,'(''e_den_w(iwalk),p,q,e_den_true,noise'',9f10.6)') e_den_w(iwalk),p,q,e_den_true,noise
!         !stop 'e_den_w(iwalk).lt.0'
!       endif
        walk_wt(iwalk)=walk_wt(iwalk)*min(max((1+tau*(e_trial-e_loc)),0.1),10._rk)*reweight_factor_inv
        !walk_wt(iwalk)=walk_wt(iwalk)*exp(tau*(e_trial-e_loc))*reweight_factor_inv
! Allow walk_wt to be negative at times
        call random_number(ran)
        ! write(6,'(''ran='',f10.5)') ran
        walk_wt(iwalk)=abs(walk_wt(iwalk))*sign(1._rk,ran-0.5*fraction_negative_wt)
      enddo

      if(method.eq.'split_join') then
        call split_join(walk_wt, e_num_w, e_den_w, nwalk, MWALK)
       elseif(method.eq.'comb') then
        call comb(walk_wt, e_num_w, e_den_w, nwalk)
       else
        stop 'method must be split_join or comb'
      endif

      do iwalk=1,nwalk
        nwalk_passes=nwalk_passes+1

        e_num=walk_wt(iwalk)*e_num_w(iwalk)
        e_den=walk_wt(iwalk)*e_den_w(iwalk)

        !e_num=e_num_prev*p-2*q+(0.5d0-rannyu(0))*noise
        !e_den=e_den_prev*p+  q+(0.5d0-rannyu(0))*noise
        !write(6,'(''e_num,e_den='',2f10.5)') e_num, e_den
        !if(e_den.lt.0._rk) write(6,'(''iblk, istep, e_den='',2i9,f10.5)') iblk, istep, e_den

        e_num_abs=e_num*sign(1._rk,e_den)
        e_num_abs_del=e_num_abs-e_num_abs_ave
        e_num_abs_ave=e_num_abs_ave+e_num_abs_del/nwalk_passes
        e_num_abs_vn1=e_num_abs_vn1+(e_num_abs-e_num_abs_ave)*e_num_abs_del

        e_den_abs=abs(e_den)
        e_den_abs_del=e_den_abs-e_den_abs_ave
        e_den_abs_ave=e_den_abs_ave+e_den_abs_del/nwalk_passes
        e_den_abs_vn1=e_den_abs_vn1+(e_den_abs-e_den_abs_ave)*e_den_abs_del

        e_num_abs_e_den_abs_vn1=e_num_abs_e_den_abs_vn1+(e_num_abs-e_num_abs_ave)*e_den_abs_del

        e_num_gen=e_num_gen+e_num
        e_den_gen=e_den_gen+e_den

        e_num_prev=e_num
        e_den_prev=e_den

      enddo ! iwalk

      passes=(iblk-1)*nstep+istep

      nwalk_blk=nwalk_blk+nwalk
      w_abs_gen=sum(abs(walk_wt(1:nwalk)))
      w_abs_blk=w_abs_blk+w_abs_gen

      e_gen=e_num_gen/e_den_gen

      e_gen_del=e_gen-e_gen_ave
      e_gen_ave=e_gen_ave+e_gen_del/passes
      e_gen_vn1=e_gen_vn1+(e_gen-e_gen_ave)*e_gen_del

      e_num_genabs=e_num_gen*sign(1._rk,e_den_gen) 
      e_num_genabs_del=e_num_genabs-e_num_genabs_ave
      e_num_genabs_ave=e_num_genabs_ave+e_num_genabs_del/passes
      e_num_genabs_vn1=e_num_genabs_vn1+(e_num_genabs-e_num_genabs_ave)*e_num_genabs_del

      e_den_genabs=abs(e_den_gen) 
      e_den_genabs_del=e_den_genabs-e_den_genabs_ave
      e_den_genabs_ave=e_den_genabs_ave+e_den_genabs_del/passes
      e_den_genabs_vn1=e_den_genabs_vn1+(e_den_genabs-e_den_genabs_ave)*e_den_genabs_del

      e_num_genabs_e_den_genabs_vn1=e_num_genabs_e_den_genabs_vn1+(e_num_genabs-e_num_genabs_ave)*e_den_genabs_del

      e_num_blk=e_num_blk+e_num_gen
      e_den_blk=e_den_blk+e_den_gen

!egr      e_gro_num_gen=sum(walk_wt(1:nwalk))
!egr      e_gro_num_blk=e_gro_num_blk+e_gro_num_gen
!egr      e_gro_den_blk=e_gro_den_blk+e_gro_den_gen

      do i=1,n_pop_control_corrected
        e_num_corrected_blk(i)=e_num_corrected_blk(i)+reweight_factor_prod(i)*e_num_gen
        e_den_corrected_blk(i)=e_den_corrected_blk(i)+reweight_factor_prod(i)*e_den_gen
!       if(i.eq.1) write(6,'(''e_num_gen,e_num_blk,e_num_corrected_blk(i),reweight_factor_prod(i)'',9f14.6)') e_num_gen,e_num_blk,e_num_corrected_blk(i),reweight_factor_prod(i)
!egr        e_gro_num_corrected_blk(i)=e_gro_num_corrected_blk(i)+reweight_factor_prod(i)*e_gro_num_gen
!egr        e_gro_den_corrected_blk(i)=e_gro_den_corrected_blk(i)+reweight_factor_prod(i)*e_gro_den_gen
      enddo

      if(passes.ge.2) then
        e_genpp_del=e_gen-e_genpp_ave
        e_genpp_ave=e_genpp_ave+e_genpp_del/(passes-1)
        e_genp_e_genpp_vn1=e_genp_e_genpp_vn1+(e_genp-e_genp_ave)*e_genpp_del
        if(passes.ge.3) then
          tau_corr_nonint=-1/log(((passes-1)/(passes-2))*e_genp_e_genpp_vn1/e_gen_vn1)
        else
          tau_corr_nonint=0
        endif
        t_corr_nonint=1+2*tau_corr_nonint
        if(e_genp_e_genpp_vn1.le.0.d0 .or. e_gen_vn1.le.0.d0 .or. e_genp_e_genpp_vn1.gt.e_gen_vn1) write(6,'(''e_genp_e_genpp_vn1, e_gen_vn1='',9es12.4)') e_genp_e_genpp_vn1, e_gen_vn1
      endif
      e_genp=e_gen
      e_genp_ave=e_gen_ave

    enddo ! istep

    nwalk_run=nwalk_run+nwalk_blk
    w_abs_run=w_abs_run+w_abs_blk
    !w_abs_blk2_run=w_abs_blk2_run+w_abs_blk**2

    e_abs_ave=e_num_abs_ave/e_den_abs_ave
    e_num_abs_var=e_num_abs_vn1/(nwalk_passes-1)
    e_den_abs_var=e_den_abs_vn1/(nwalk_passes-1)
    e_num_abs_e_den_abs_var=e_num_abs_e_den_abs_vn1/(nwalk_passes-1)
    ratio(1)=(e_den_abs_var/e_den_abs_ave**2-(e_num_abs_e_den_abs_var/(e_num_abs_ave*e_den_abs_ave)))/nwalk_passes ! Remove 1st-order bias in ratio of averages
    e_abs_ave=e_abs_ave/(1+(e_den_abs_var/e_den_abs_ave**2-(e_num_abs_e_den_abs_var/(e_num_abs_ave*e_den_abs_ave)))/nwalk_passes) ! Remove 1st-order bias in ratio of averages
    e_abs_err=abs(e_num_abs_ave/e_den_abs_ave)*sqrt((e_num_abs_var/e_num_abs_ave**2 + e_den_abs_var/e_den_abs_ave**2 -2*e_num_abs_e_den_abs_var/(e_num_abs_ave*e_den_abs_ave))/nwalk_passes)

    e_genabs_ave=e_num_genabs_ave/e_den_genabs_ave
    e_num_genabs_var=e_num_genabs_vn1/(passes-1)
    e_den_genabs_var=e_den_genabs_vn1/(passes-1)
    e_num_genabs_e_den_genabs_var=e_num_genabs_e_den_genabs_vn1/(passes-1)
    ratio(2)=(e_den_genabs_var/e_den_genabs_ave**2-(e_num_genabs_e_den_genabs_var/(e_num_genabs_ave*e_den_genabs_ave)))/passes ! Remove 1st-order bias in ratio of averages
    e_genabs_ave=e_genabs_ave/(1+(e_den_genabs_var/e_den_genabs_ave**2-(e_num_genabs_e_den_genabs_var/(e_num_genabs_ave*e_den_genabs_ave)))/passes) ! Remove 1st-order bias in ratio of averages
    e_genabs_err=abs(e_num_genabs_ave/e_den_genabs_ave)*sqrt((e_num_genabs_var/e_num_genabs_ave**2 + e_den_genabs_var/e_den_genabs_ave**2 -2*e_num_genabs_e_den_genabs_var/(e_num_genabs_ave*e_den_genabs_ave))/passes)

    e_blk=e_num_blk/e_den_blk

    e_num_blkabs=e_num_blk*sign(1._rk,e_den_blk) 
    e_num_blkabs_del=e_num_blkabs-e_num_blkabs_ave
    e_num_blkabs_ave=e_num_blkabs_ave+e_num_blkabs_del/iblk
    e_num_blkabs_vn1=e_num_blkabs_vn1+(e_num_blkabs-e_num_blkabs_ave)*e_num_blkabs_del

    e_den_blkabs=abs(e_den_blk)
    e_den_blkabs_del=e_den_blkabs-e_den_blkabs_ave
    e_den_blkabs_ave=e_den_blkabs_ave+e_den_blkabs_del/iblk
    e_den_blkabs_vn1=e_den_blkabs_vn1+(e_den_blkabs-e_den_blkabs_ave)*e_den_blkabs_del

    e_num_blkabs_e_den_blkabs_vn1=e_num_blkabs_e_den_blkabs_vn1+(e_num_blkabs-e_num_blkabs_ave)*e_den_blkabs_del

    do i=1,n_pop_control_corrected
      e_num_corrected_blkabs(i)=e_num_corrected_blk(i)*sign(1._rk,e_den_corrected_blk(i))
      e_num_corrected_blkabs_del(i)=e_num_corrected_blkabs(i)-e_num_corrected_blkabs_ave(i)
      e_num_corrected_blkabs_ave(i)=e_num_corrected_blkabs_ave(i)+e_num_corrected_blkabs_del(i)/iblk
      e_num_corrected_blkabs_vn1(i)=e_num_corrected_blkabs_vn1(i)+(e_num_corrected_blkabs(i)-e_num_corrected_blkabs_ave(i))*e_num_corrected_blkabs_del(i)

      e_den_corrected_blkabs(i)=abs(e_den_corrected_blk(i))
      e_den_corrected_blkabs_del(i)=e_den_corrected_blkabs(i)-e_den_corrected_blkabs_ave(i)
      e_den_corrected_blkabs_ave(i)=e_den_corrected_blkabs_ave(i)+e_den_corrected_blkabs_del(i)/iblk
      e_den_corrected_blkabs_vn1(i)=e_den_corrected_blkabs_vn1(i)+(e_den_corrected_blkabs(i)-e_den_corrected_blkabs_ave(i))*e_den_corrected_blkabs_del(i)

      e_num_corrected_blkabs_e_den_corrected_blkabs_vn1(i)=e_num_corrected_blkabs_e_den_corrected_blkabs_vn1(i)+(e_num_corrected_blkabs(i)-e_num_corrected_blkabs_ave(i))*e_den_corrected_blkabs_del(i)
    enddo
    !write(6,'(''e_num_blkabs_vn1='',100es14.6)') e_num_blkabs_vn1,(e_num_corrected_blkabs_vn1(i),i=1,n_pop_control_corrected)
    !write(6,'(''e_den_blkabs_vn1='',100es14.6)') e_den_blkabs_vn1,(e_den_corrected_blkabs_vn1(i),i=1,n_pop_control_corrected)
    !write(6,'(''e_num_blkabs_e_den_blkabs_vn1='',100es14.6)') e_num_blkabs_e_den_blkabs_vn1,(e_num_corrected_blkabs_e_den_corrected_blkabs_vn1(i),i=1,n_pop_control_corrected)

    e_num_blk_del=e_num_blk-e_num_blk_ave
    e_num_blk_ave=e_num_blk_ave+e_num_blk_del/iblk
    e_num_blk_vn1=e_num_blk_vn1+(e_num_blk-e_num_blk_ave)*e_num_blk_del

    e_den_blk_del=e_den_blk-e_den_blk_ave
    e_den_blk_ave=e_den_blk_ave+e_den_blk_del/iblk
    e_den_blk_vn1=e_den_blk_vn1+(e_den_blk-e_den_blk_ave)*e_den_blk_del

    e_num_blk_e_den_blk_vn1=e_num_blk_e_den_blk_vn1+(e_num_blk-e_num_blk_ave)*e_den_blk_del

    e_blkabs_ave=e_num_blkabs_ave/e_den_blkabs_ave
    e_blk_ave=e_num_blk_ave/e_den_blk_ave
    !write(6, '(''tmp1'', 100es12.4)') e_blkabs_err, e_num_blkabs_ave, e_den_blkabs_ave, e_num_blkabs_var, e_den_blkabs_var, e_num_blkabs_e_den_blkabs_var, e_num_blkabs_var/e_num_blkabs_ave**2, e_den_blkabs_var/e_den_blkabs_ave**2, e_num_blkabs_e_den_blkabs_var/(e_num_blkabs_ave*e_den_blkabs_ave), (e_num_blkabs_var/e_num_blkabs_ave**2 + e_den_blkabs_var/e_den_blkabs_ave**2 -2*e_num_blkabs_e_den_blkabs_var/(e_num_blkabs_ave*e_den_blkabs_ave))
    write(6,'(''iblkk,iblk='',9i5)') iblkk,iblk ; call flush(6)

    if(iblk.ge.2) then
      e_num_blkabs_var=e_num_blkabs_vn1/(iblk-1)
      e_den_blkabs_var=e_den_blkabs_vn1/(iblk-1)
      e_num_blkabs_e_den_blkabs_var=e_num_blkabs_e_den_blkabs_vn1/(iblk-1)
      ratio(3)=(e_den_blkabs_var/e_den_blkabs_ave**2-(e_num_blkabs_e_den_blkabs_var/(e_num_blkabs_ave*e_den_blkabs_ave)))/iblk ! Remove 1st-order bias in ratio of averages
      e_blkabs_ave=e_blkabs_ave/(1+(e_den_blkabs_var/e_den_blkabs_ave**2-(e_num_blkabs_e_den_blkabs_var/(e_num_blkabs_ave*e_den_blkabs_ave)))/iblk) ! Remove 1st-order bias in ratio of averages
      e_blkabs_err=abs(e_num_blkabs_ave/e_den_blkabs_ave)*sqrt((e_num_blkabs_var/e_num_blkabs_ave**2 + e_den_blkabs_var/e_den_blkabs_ave**2 -2*e_num_blkabs_e_den_blkabs_var/(e_num_blkabs_ave*e_den_blkabs_ave))/iblk)
      t_corr=(e_blkabs_err/e_genabs_err)**2
      do i=1,n_pop_control_corrected
        e_num_corrected_blkabs_var(i)=e_num_corrected_blkabs_vn1(i)/(iblk-1)
        e_den_corrected_blkabs_var(i)=e_den_corrected_blkabs_vn1(i)/(iblk-1)
        e_num_corrected_blkabs_e_den_corrected_blkabs_var(i)=e_num_corrected_blkabs_e_den_corrected_blkabs_vn1(i)/(iblk-1)
        e_corrected_blkabs_ave(i)=e_num_corrected_blkabs_ave(i)/e_den_corrected_blkabs_ave(i)
        e_corrected_blkabs_ave(i)=e_corrected_blkabs_ave(i)/(1+(e_den_corrected_blkabs_var(i)/e_den_corrected_blkabs_ave(i)**2-(e_num_corrected_blkabs_e_den_corrected_blkabs_var(i)/(e_num_corrected_blkabs_ave(i)*e_den_corrected_blkabs_ave(i))))/iblk) ! Remove 1st-order bias in ratio of averages
        e_corrected_blkabs_err(i)=abs(e_num_corrected_blkabs_ave(i)/e_den_corrected_blkabs_ave(i))*sqrt((e_num_corrected_blkabs_var(i)/e_num_corrected_blkabs_ave(i)**2 + e_den_corrected_blkabs_var(i)/e_den_corrected_blkabs_ave(i)**2 -2*e_num_corrected_blkabs_e_den_corrected_blkabs_var(i)/(e_num_corrected_blkabs_ave(i)*e_den_corrected_blkabs_ave(i)))/iblk)
        !write(6, '(''tmp2'', 100es12.4)') e_corrected_blkabs_err(i), e_num_corrected_blkabs_ave(i), e_den_corrected_blkabs_ave(i), e_num_corrected_blkabs_var(i), e_den_corrected_blkabs_var(i), e_num_corrected_blkabs_e_den_corrected_blkabs_var(i), e_num_corrected_blkabs_var(i)/e_num_corrected_blkabs_ave(i)**2, e_den_corrected_blkabs_var(i)/e_den_corrected_blkabs_ave(i)**2, e_num_corrected_blkabs_e_den_corrected_blkabs_var(i)/(e_num_corrected_blkabs_ave(i)*e_den_corrected_blkabs_ave(i)), (e_num_corrected_blkabs_var(i)/e_num_corrected_blkabs_ave(i)**2 + e_den_corrected_blkabs_var(i)/e_den_corrected_blkabs_ave(i)**2 -2*e_num_corrected_blkabs_e_den_corrected_blkabs_var(i)/(e_num_corrected_blkabs_ave(i)*e_den_corrected_blkabs_ave(i)))
      enddo ! n_pop_control_corrected

!Warning: indent if these lines are kept
!! The best energy is the one with the largest undoing of the population control for which the slope is getting monotonically less negative.
! The best energy is the one with the largest undoing of the population control for which the energy is getting monotonically going wown.
! First assume that the last is best and then if it start going up set it to previous point and exit
  e_corrected_blkabs_ave_best=e_corrected_blkabs_ave(n_pop_control_corrected)
  e_corrected_blkabs_err_best=e_corrected_blkabs_err(n_pop_control_corrected)
  i_pop_control_corrected_best=n_pop_control_corrected
  do i=2,n_pop_control_corrected
    e_change=e_corrected_blkabs_ave(i)-e_corrected_blkabs_ave(i-1)
    e_slope=e_change/(1/log(reweight_factor_pow(i-1))-1/log(reweight_factor_pow(i)))
    !if(e_change.gt.0_rk .or. (e_slope/e_slope_prev.gt.1.2_rk .and. i.ge.3)) then
    if(e_corrected_blkabs_ave(i)+2*e_corrected_blkabs_err(i).gt.e_corrected_blkabs_ave(i-1)+2*e_corrected_blkabs_err(i-1)) then
      e_corrected_blkabs_ave_best=e_corrected_blkabs_ave(i-1)
      e_corrected_blkabs_err_best=e_corrected_blkabs_err(i-1)
      i_pop_control_corrected_best=i-1
      exit
    endif
    e_slope_prev=e_slope
  enddo
  write(6,'(''For undoing pop control, i_pop_control_corrected_best, power, number of generations='',i5,f8.4,f8.1)') &
 & i_pop_control_corrected_best, reweight_factor_pow(i_pop_control_corrected_best), -1/log(reweight_factor_pow(i_pop_control_corrected_best))
  write(6,'(''Pop_control uncorrected, corrected energy='',2(f13.8,''(''i8,'')''),'' t_corr_nonint, t_corr, nstep='',2f8.2,i6)') &
 &  e_blkabs_ave, nint(100000000*e_blkabs_err), e_corrected_blkabs_ave_best, nint(100000000*e_corrected_blkabs_err_best),&
 &  t_corr_nonint, t_corr, nstep

      e_num_blk_var=e_num_blk_vn1/(iblk-1)
      e_den_blk_var=e_den_blk_vn1/(iblk-1)
      e_num_blk_e_den_blk_var=e_num_blk_e_den_blk_vn1/(iblk-1)
      ratio(4)=(e_den_blk_var/e_den_blk_ave**2-(e_num_blk_e_den_blk_var/(e_num_blk_ave*e_den_blk_ave)))/iblk ! Remove 1st-order bias in ratio of averages
      e_blk_ave=e_blk_ave/(1+(e_den_blk_var/e_den_blk_ave**2-(e_num_blk_e_den_blk_var/(e_num_blk_ave*e_den_blk_ave)))/iblk) ! Remove 1st-order bias in ratio of averages
      e_blk_err=abs(e_num_blk_ave/e_den_blk_ave)*sqrt((e_num_blk_var/e_num_blk_ave**2 + e_den_blk_var/e_den_blk_ave**2 -2*e_num_blk_e_den_blk_var/(e_num_blk_ave*e_den_blk_ave))/iblk)
    else
      e_blkabs_err=0
      t_corr=0
      e_blk_err=0
    endif ! iblk=2

    e_num_cum=e_num_cum+e_num_blk
    e_den_cum=e_den_cum+e_den_blk

!   e_abs_ave=e_abs_ave/(1+(e_den_abs_var/e_den_abs_ave**2-(e_num_abs_e_den_abs_var/(e_num_abs_ave*e_den_abs_ave)))/nwalk_passes) ! Remove 1st-order bias in ratio of averages
!   e_genabs_ave=e_genabs_ave/(1+(e_den_genabs_var/e_den_genabs_ave**2-(e_num_genabs_e_den_genabs_var/(e_num_genabs_ave*e_den_genabs_ave)))/passes) ! Remove 1st-order bias in ratio of averages
!     e_blkabs_ave=e_blkabs_ave/(1+(e_den_blkabs_var/e_den_blkabs_ave**2-(e_num_blkabs_e_den_blkabs_var/(e_num_blkabs_ave*e_den_blkabs_ave)))/iblk) ! Remove 1st-order bias in ratio of averages
!     e_blk_ave=e_blk_ave/(1+(e_den_blk_var/e_den_blk_ave**2-(e_num_blk_e_den_blk_var/(e_num_blk_ave*e_den_blk_ave)))/iblk) ! Remove 1st-order bias in ratio of averages
    !write(6,'(''4 ratios:'',9es16.8)') ratio(1:4)
    !write(6,'(''4 ratios:'',9es16.8)') (e_den_abs_var/e_den_abs_ave**2-(e_num_abs_e_den_abs_var/(e_num_abs_ave*e_den_abs_ave)))/nwalk_passes, (e_den_genabs_var/e_den_genabs_ave**2-(e_num_genabs_e_den_genabs_var/(e_num_genabs_ave*e_den_genabs_ave)))/passes, (e_den_blkabs_var/e_den_blkabs_ave**2-(e_num_blkabs_e_den_blkabs_var/(e_num_blkabs_ave*e_den_blkabs_ave)))/iblk, (e_den_blk_var/e_den_blk_ave**2-(e_num_blk_e_den_blk_var/(e_num_blk_ave*e_den_blk_ave)))/iblk
    !write(6,'(''e_num_abs_ave, e_num_genabs_ave, e_num_blkabs_ave, e_num_blk_ave'',9es16.8)') e_num_abs_ave, e_num_genabs_ave, e_num_blkabs_ave, e_num_blk_ave, e_den_abs_ave, e_den_genabs_ave, e_den_blkabs_ave, e_den_blk_ave
    write(6,'(''iblk, w_perm_initiator, nwalk, w_abs, w_abs_imp='',i6,f9.1,i8,i9,i9,'' e_blk='',f10.4,'' e='',4(f14.8,''(''i8,'')''), '' e_trial, rew_fac_inv, t_c='',2f11.5,f8.1)') iblk, w_perm_initiator_blk/nstep, nwalk, nint(w_abs_blk/nstep), nint(w_abs_blk_imp/nstep), &
 &  e_blk, e_abs_ave, nint(100000000*e_abs_err), e_genabs_ave, nint(100000000*e_genabs_err), e_blkabs_ave, nint(100000000*e_blkabs_err), e_blk_ave, nint(100000000*e_blk_err), e_trial, reweight_factor_inv, t_corr_nonint

  enddo ! iblk

! write(6,'(''w_perm_initiator_av, nwalk_av, nwalk_before_merge_av, ratio, w_av, w_abs_av, w_abs_before_merge_av, ratio, e_den_genabs_av, e_den_blkabs_av/nstep='',f8.1,2f10.1,f6.2,3f10.1,f6.2,9f10.1)') &
!&w_perm_initiator_cum/passes, nwalk_cum/passes, nwalk_before_merge_cum/passes, dfloat(nwalk_cum)/nwalk_before_merge_cum, w_cum/passes, w_abs_cum/passes, w_abs_before_merge_cum/passes, dfloat(w_abs_cum)/w_abs_before_merge_cum, e_den_genabs_av, e_den_blkabs_av/nstep
  write(6,'(''nwalk_av, w_abs_av='',9f8.1)') real(nwalk_run,rk)/real(passes,rk), w_abs_run/passes

! write(6,'(''Energy='',4(f11.6,''(''i6,'')''),'' energy_exact='',f10.5,'' t_corr_nonint, t_corr, nstep='',2f8.2,i6)') &
  write(6,'(''Energy='',4(f13.8,''(''i8,'')''),'' t_corr_nonint, t_corr, nstep='',2f8.2,i6)') &
 &  e_abs_ave, nint(100000000*e_abs_err), e_genabs_ave, nint(100000000*e_genabs_err), e_blkabs_ave, nint(100000000*e_blkabs_err), &
!&  e_ave, nint(100000000*e_err), energy_exact, t_corr_nonint, t_corr, nstep
 &  e_blk_ave, nint(100000000*e_blk_err), t_corr_nonint, t_corr, nstep

  write(6,'('' i    e_corrected(  err   )    difference    slope'')')
  write(6,'(3x,f14.8,''(''i8,'')'',2f14.8)') e_blkabs_ave, nint(100000000*e_blkabs_err)
  do i=1,n_pop_control_corrected
    if(i.eq.1) then
      write(6,'(i3,f14.8,''(''i8,'')'',2f14.8)') i, e_corrected_blkabs_ave(i), nint(100000000*e_corrected_blkabs_err(i)), &
 &    e_corrected_blkabs_ave(i)-e_blkabs_ave
    else
      write(6,'(i3,f14.8,''(''i8,'')'',2f14.8)') i, e_corrected_blkabs_ave(i), nint(100000000*e_corrected_blkabs_err(i)), &
 &    e_corrected_blkabs_ave(i)-e_corrected_blkabs_ave(i-1), (e_corrected_blkabs_ave(i)-e_corrected_blkabs_ave(i-1))/(1/log(reweight_factor_pow(i-1))-1/log(reweight_factor_pow(i)))
    endif
  enddo

  write(6,'(''At end of run, reweight_factor_prod='',1000es9.2)') (reweight_factor_prod(i),i=1,n_pop_control_corrected)

!! The best energy is the one with the largest undoing of the population control for which the slope is getting monotonically less negative.
! The best energy is the one with the largest undoing of the population control for which the energy is getting monotonically going wown.
! First assume that the last is best and then if it start going up set it to previous point and exit
  e_corrected_blkabs_ave_best=e_corrected_blkabs_ave(n_pop_control_corrected)
  e_corrected_blkabs_err_best=e_corrected_blkabs_err(n_pop_control_corrected)
  i_pop_control_corrected_best=n_pop_control_corrected
  do i=2,n_pop_control_corrected
    e_change=e_corrected_blkabs_ave(i)-e_corrected_blkabs_ave(i-1)
    e_slope=e_change/(1/log(reweight_factor_pow(i-1))-1/log(reweight_factor_pow(i)))
    !if(e_change.gt.0_rk .or. (e_slope/e_slope_prev.gt.1.2_rk .and. i.ge.3)) then
    if(e_corrected_blkabs_ave(i)+2*e_corrected_blkabs_err(i).gt.e_corrected_blkabs_ave(i-1)+2*e_corrected_blkabs_err(i-1)) then
      e_corrected_blkabs_ave_best=e_corrected_blkabs_ave(i-1)
      e_corrected_blkabs_err_best=e_corrected_blkabs_err(i-1)
      i_pop_control_corrected_best=i-1
      exit
    endif
    e_slope_prev=e_slope
  enddo
  write(6,'(''For undoing pop control, i_pop_control_corrected_best, power, number of generations='',i5,f8.4,f8.1)') &
 & i_pop_control_corrected_best, reweight_factor_pow(i_pop_control_corrected_best), -1/log(reweight_factor_pow(i_pop_control_corrected_best))
  write(6,'(''Pop_control uncorrected, corrected energy='',2(f13.8,''(''i8,'')''),'' t_corr_nonint, t_corr, nstep='',2f8.2,i6)') &
 &  e_blkabs_ave, nint(100000000*e_blkabs_err), e_corrected_blkabs_ave_best, nint(100000000*e_corrected_blkabs_err_best),&
 &  t_corr_nonint, t_corr, nstep

  call my_second(2,'run')

end subroutine walk
!---------------------------------------------------------------------------------------------------------------------

  subroutine split_join(walk_wt,e_num_w,e_den_w,nwalk,MWALK)
! ==============================================================================
! Description   : Do branching using split_join algorithm
! Author        : Cyrus Umrigar
! ------------------------------------------------------------------------------
  use types, only: i1b, ik, rk
  implicit none
  integer, intent(in) :: MWALK
  integer, intent(inout) :: nwalk
  real(rk), intent(inout) :: walk_wt(MWALK), e_num_w(MWALK), e_den_w(MWALK)
  integer iwalk, iwalk2, nwalk2, nwalk_after_join, nwalk_max_dupl, j, iunder, ipair, nbrnch
  real(rk) rannyu, w_sum, w_sum2, wtot, w_split, w_sav(MWALK)
! real(rk) w_partial_sum(nwalk), e_tmp(nwalk)
  integer iwundr(nwalk)

! if(ipr.ge.2) then
!   write(6,'(''nwalk='',9i5)') nwalk
!   write(6,'(''walk_wt='',100f6.2)') (walk_wt(iwalk),iwalk=1,nwalk)
! endif

! Identify the walkers that are to be killed because their walk_wt. is , or that are
! to be merged because their ws are less than 1/2.  For each pair whose ws are < 1/2
! give the one that is to be kept the additional walk_wt of the other and put the other one on a stack.
  iunder=0
  ipair=0
  w_sum=0._rk
  !write(6,'(''before join walk_wt='',1000f6.2)') (walk_wt(iwalk),iwalk=1,nwalk)
  do iwalk=1,nwalk
    w_sum=w_sum+walk_wt(iwalk)
    if(walk_wt(iwalk).lt.0.5_rk) then
      if(walk_wt(iwalk).eq.0._rk) then
        nbrnch=nbrnch+1
        iunder=iunder+1
        iwundr(iunder)=iwalk
       else
        if(ipair.eq.0) then
          ipair=1
          iwalk2=iwalk
         else
          nbrnch=nbrnch+1
          ipair=0
          iunder=iunder+1
          wtot=walk_wt(iwalk)+walk_wt(iwalk2)
          if(rannyu(0).gt.(walk_wt(iwalk)/wtot)) then
            walk_wt(iwalk2)=wtot
            iwundr(iunder)=iwalk
           else
            walk_wt(iwalk)=wtot
            iwundr(iunder)=iwalk2
          endif
        endif
      endif
    endif
  enddo

! Figure out what weight walkers can be split without exceeding MWALK
! nwalk_after_join=nwalk-iunder
! nwalk_max_dupl=MWALK-nwalk_after_join
! do iwalk=1,nwalk
!   w_sav(iwalk)=walk_wt(iwalk)
! enddo
! call shell(w_sav,nwalk)
! w_split=w_sav(max(1,nwalk-nwalk_max_dupl))

! For now just split walkers with weight <=2
  w_split=2._rk

! Split the walkers whose walk_wt is >max(1._rk,w_split).  If there are walkers that were eliminated, so that iunder>0
! then put the new walker in that location.  Otherwise put it at the end.
  nwalk2=nwalk
  do iwalk=1,nwalk
    if(walk_wt(iwalk).gt.max(2._rk,w_split)) then
      nbrnch=nbrnch+1
      if(iunder.gt.0) then
        iwalk2=iwundr(iunder)
        iunder=iunder-1
       else
        nwalk2=nwalk2+1
        iwalk2=nwalk2
        if(nwalk2.gt.MWALK) then
          write(6,'(''iwalk,nwalk,nwalk2,MWALK='',9i5)') iwalk,nwalk,nwalk2,MWALK
          write(6,'(''walk_wt='',100f6.2)') walk_wt(1:nwalk)
          stop 'MWALK exceeded in splitj_movall'
        endif
      endif
      walk_wt(iwalk)=walk_wt(iwalk)*0.5_rk
      walk_wt(iwalk2)=walk_wt(iwalk)
      e_num_w(iwalk2)=e_num_w(iwalk)
      e_den_w(iwalk2)=e_den_w(iwalk)
    endif
  enddo

! If more walkers were eliminated than the number duplicated then consolidate
! the remaining walkers so that they are they occupy the first positions.
  do j=iunder,1,-1
    iwalk2=iwundr(j)
    iwalk=nwalk2
    nwalk2=nwalk2-1
    walk_wt(iwalk2)=walk_wt(iwalk)
    e_num_w(iwalk2)=e_num_w(iwalk)
    e_den_w(iwalk2)=e_den_w(iwalk)
  enddo
  nwalk=nwalk2

  w_sum2=sum(walk_wt(1:nwalk))
  if(dabs(w_sum-w_sum2).gt.1.d-12*dabs(w_sum)) write(11,'(2f14.6)') w_sum,w_sum2
  !write(6,'(''after split walk_wt='',1000f6.2)') (walk_wt(iwalk),iwalk=1,nwalk)

  return
  end subroutine split_join
!---------------------------------------------------------------------------------------------------------------------
  subroutine comb(w,e_num_w,e_den_w,nwalk)
! ==============================================================================
! Description   : Do branching using comb algorithm
! Author        : Cyrus Umrigar
! ------------------------------------------------------------------------------
  use types, only: i1b, ik, rk
  implicit none
  integer, intent(in) :: nwalk
  real(rk), intent(inout) :: w(nwalk), e_num_w(nwalk), e_den_w(nwalk)
  integer iwalk, iwalk_old, iwalk_old_begin
  real(rk) comb_tine, random, rannyu
  real(rk) w_partial_sum(nwalk), e_num_tmp(nwalk), e_den_tmp(nwalk)

! Set cumulative probabilities
  do iwalk=1,nwalk
    if(iwalk.eq.1) then
      w_partial_sum(iwalk)=w(iwalk)
     else
      w_partial_sum(iwalk)=w_partial_sum(iwalk-1)+w(iwalk)
    endif
  enddo

  if(ipr.ge.1) then
    write(6,'(/,''wo           ='',1000f10.5)') w(1:nwalk)
    write(6,'(''w_partial_sum='',1000f10.5)') w_partial_sum(1:nwalk)
    write(6,'(''e_num_old     ='',1000f10.5)') e_num_w(1:nwalk)
    write(6,'(''e_den_old     ='',1000f10.5)') e_num_w(1:nwalk)
  endif

  random=rannyu(0)
  random=mod(random,1._rk/nwalk) ! This is a random number between 0 and 1/nwalk
  iwalk_old_begin=1
  do iwalk=1,nwalk
    comb_tine=random+dfloat(iwalk-1)/nwalk
    do iwalk_old=iwalk_old_begin,nwalk
       if(w_partial_sum(iwalk_old).gt.comb_tine*w_partial_sum(nwalk)) then
         iwalk_old_begin=iwalk_old
         exit
       endif
    enddo
    e_num_tmp(iwalk)=e_num_w(iwalk_old_begin)
    e_den_tmp(iwalk)=e_den_w(iwalk_old_begin)
    if(iwalk_old_begin.gt.nwalk) stop 'iwalk_old_begin > nwalk'
  enddo
  w=w_partial_sum(nwalk)/nwalk
  e_num_w=e_num_tmp
  e_den_w=e_den_tmp

  if(ipr.ge.1) then
    write(6,'(/,''wn           ='',1000f10.5)') w(1:nwalk)
    write(6,'(''e_num_new    ='',1000f10.5)') e_num_w(1:nwalk)
    write(6,'(''e_den_new    ='',1000f10.5)') e_den_w(1:nwalk)
  endif

  call flush(6)

  end subroutine comb

!end module population_control
end program template
