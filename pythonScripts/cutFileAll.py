import math
import numpy as np
import matplotlib.pyplot as plt
import sys
#Arguments:
#fileName runNumber numberOfPoints
#fileName       - name of the file to be cut
#runNumber      - the number of the run; sets the start point for the cut
#numberOfPoints - number of points to output to data file
points    = np.loadtxt(str(sys.argv[1]))
#runNumber = int(sys.argv[2])
nmbPoints = int(sys.argv[2])
data      = np.zeros(nmbPoints)

for j in range(75,2150):
    for i in range(nmbPoints):
        data[i] = points[j * nmbPoints + i]
    np.savetxt('cutFile' + str(j) + '.dat',data)
