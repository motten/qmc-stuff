# coding: utf-8
import math
import numpy as np
import matplotlib.pyplot as plt
import sys
#Command Line Arguments:
#fileName subtitle
#fileName       - name of the file to be analyzed
#subtitle       - descriptive subtitle
#
points   = np.loadtxt(str(sys.argv[1]))
subTitle = str(sys.argv[2])
numberOfPoints = float(sys.argv[3])
blockPoints = np.zeros((points.size/2 - 2,2))
points[:,1] = points[:,1] * np.sqrt(numberOfPoints)
blockPoints = points[4:,:]

error   = np.zeros(blockPoints.size/6)
for i in range(error.size):
    error[i] = blockPoints[i,1]/np.sqrt(2*(points[4,0]*2/blockPoints[i,0] - 1))
    
plt.figure(0)    
#plt.clf()
plt.errorbar(np.log2(blockPoints[0:blockPoints.size/6,0]),blockPoints[0:blockPoints.size/6,1],yerr=error,label='Blocking')
plt.errorbar(np.log2(blockPoints[0:blockPoints.size/6,0]),blockPoints[blockPoints.size/6:blockPoints.size/3,1],yerr=error,label='Exp(1) Blocking')
plt.errorbar(np.log2(blockPoints[0:blockPoints.size/6,0]),blockPoints[(blockPoints.size/3):blockPoints.size,1],yerr=error,label='Exp(2) Blocking')
plt.axhline(y=points[0,1],color='g',label='AR(1)')
#plt.axhline(y=points[1,1],color='r',label='AR(2)')
#plt.axhline(y=points[2,1],color='black',label='AR(3)')
plt.axhline(y=points[3,1],color='m',label='NL')
plt.legend(loc='upper left')
plt.xlabel('log2(numberInBlock)')
plt.ylabel('Error')
plt.title('Error for Blocking, NonLinear, and AR Models\n' + subTitle)
plt.show()
