#Recasts an arbitrary function as an AR(p) model
# coding: utf-8
import math
import numpy as np
import matplotlib.pyplot as plt
import sys
p = int(sys.argv[1])
parameterA = float(sys.argv[2])
parameterB = float(sys.argv[3])
parameterC = 1 - parameterA
parameterD = float(sys.argv[4])

def acf(lag):
    return parameterA*math.exp(-parameterB*abs(lag)) +parameterC*math.exp(-parameterD*abs(lag))

A = np.zeros((p,p))
b = np.zeros(p)
C = np.zeros((p,p),dtype=np.complex64)
a = np.zeros(p,dtype=np.complex64)

for i in range(p):
    b[i] = acf(i+1)

for i in range(p):
    for j in range(p):
        A[i,j] = acf(j-i)

x        = np.linalg.solve(A,b)
charPoly = np.zeros(x.size + 1)
roots    = np.zeros(x.size + 1)

for i in range(charPoly.size - 1):
    charPoly[i] = -x[i]

charPoly[i+1] = 1

roots = np.roots(charPoly)

for i in range(p):
    b[i] = acf(i)
    for j in range(p):
        C[i,j] = roots[j]**(-i)

a = np.linalg.solve(C,b)
#print x

xPoints = np.arange(0,1000,1)
logxPoints = np.zeros(xPoints.size)
logxPoints[1:] = np.log(xPoints[1:])
expACF  = parameterA*np.exp(-parameterB*abs(xPoints)) + parameterC*np.exp(-parameterD*abs(xPoints))
ARACF = np.zeros(expACF.size,dtype=np.complex64)
for i in range(p):
    ARACF += a[i]*roots[i]**(-xPoints)

plt.figure(0)    
plt.clf()
plt.plot(xPoints,expACF,label='Exp')
plt.plot(xPoints,ARACF,label='AR(10)')
plt.legend(loc='upper right')
plt.xlabel('Lag')
plt.ylabel('ACF')
title = 'Exp and AR(' + str(p) +') Fit ACF\nACF =' + str(parameterA) + 'exp(-' + str(parameterB) + 't) + ' + str(parameterC) + 'exp(-' + str(parameterD) + 't)'
plt.title(title)
plt.show()

plt.figure(1)    
plt.clf()
plt.plot(logxPoints,expACF,label='Exp')
plt.plot(logxPoints,ARACF,label='AR(10)')
plt.legend(loc='upper right')
plt.xlabel('log(Lag)')
plt.ylabel('ACF')
plt.title(title)
plt.show()

#Plot the integral / sum

integralExpACF = -parameterA/parameterB*np.exp(-parameterB*abs(xPoints)) - parameterC/parameterD*np.exp(-parameterD*abs(xPoints))

sumARACF = np.zeros(expACF.size,dtype=np.complex64)
for i in range(p):
    sumARACF += a[i]*1/roots[i]*(1-(1/roots[i])**(xPoints))/(1-1/roots[i])


plt.figure(2)    
plt.clf()
plt.plot(xPoints,integralExpACF,label='Exp')
plt.plot(xPoints,sumARACF,label='AR(10)')
plt.legend(loc='lower right')
plt.xlabel('t')
plt.ylabel('ACF')
title2 = 'Exp and AR(' + str(p) +') Sum ACF\nACF =' + str(parameterA) + 'exp(-' + str(parameterB) + 't) + ' + str(parameterC) + 'exp(-' + str(parameterD) + 't)'
plt.title(title2)
plt.show()

plt.figure(3)    
plt.clf()
plt.plot(logxPoints,integralExpACF,label='Exp')
plt.plot(logxPoints,sumARACF,label='AR(10)')
plt.legend(loc='lower right')
plt.xlabel('log(t)')
plt.ylabel('ACF')
plt.title(title2)
plt.show()

print integralExpACF[0]
print sumARACF[sumARACF.size - 1]
