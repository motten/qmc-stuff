# coding: utf-8
import math
import numpy as np
import matplotlib.pyplot as plt
import sys
#Command Line Arguments:
#fileName subtitle skip
#fileName       - name of the file to be analyzed
#subtitle       - descriptive subtitle
#skip           - what the counter should skip for the histogram plot
points   = np.loadtxt(str(sys.argv[1]))
subTitle = str(sys.argv[2])
skip     = int(sys.argv[3])
first = points[0,0]
for i in range(1,points.size):
    if(points[i,0] == first): break
    
period    = i
average   = np.zeros(period)
error     = np.zeros(period)
funcError = np.zeros(period)
data      = np.zeros((period,(points.size/(2*period))))
for i in range(points.size/2):
    j=i%period
    data[j,((i-j)/period)] = points[i,1]

for i in range(period):
    data[i] = np.nan_to_num(data[i])
    data[i] = np.ma.masked_equal(data[i], 0)
    
for i in range(period):
    average[i]   = data[i].mean()
    error[i]     = data[i].std()
    if i>3:
        funcError[i] = average[i] / np.sqrt(2*(points[(4+period),0]*2/points[i,0] - 1))

plt.figure(0)    
plt.clf()
plt.errorbar(np.log2(points[4:period,0]),average[4:],yerr=error[4:],label='Blocking')
plt.errorbar(np.log2(points[4:period,0]),average[4:],yerr=funcError[4:],label='Function Error')
plt.axhline(y=average[0],color='g',label='AR(1)')
plt.axhline(y=average[1],color='r',label='AR(2)')
plt.axhline(y=average[2],color='black',label='AR(3)')
plt.axhline(y=average[3],color='m',label='NonLinear')
plt.errorbar(4,average[0],yerr=error[0],color='g')
plt.errorbar(5,average[1],yerr=error[1],color='r')
plt.errorbar(6,average[2],yerr=error[2],color='black')
plt.errorbar(8.5,average[3],yerr=error[3],color='m')

plt.legend(loc='upper left')
plt.xlabel('log2(numberInBlock)')
plt.ylabel('Error')
plt.title('Error for Blocking, NonLinear, and AR Models\n' + subTitle)
plt.show()

plt.figure(1)
plt.clf()
for i in range(4,period,skip):
    name = 'Nblocks ' + str(2**(i-2))
    plt.hist(data[i],bins=200,histtype='step',label=name)

plt.hist(data[0],bins=200,label='AR(1)',histtype='step',lw='3',ls='dashed')
plt.hist(data[1],bins=200,label='AR(2)',histtype='step',lw='3',ls='dashed')
plt.hist(data[2],bins=200,label='AR(3)',histtype='step',lw='3',ls='dashed')
plt.hist(data[3],bins=200,label='NonLinear',histtype='step',lw='3',ls='dashed')

plt.xlabel('Error')
plt.ylabel('Frequency')
plt.title('Error for Blocking, NonLinear, AR Models\n' + subTitle)
plt.legend()
plt.show()

