import math
import numpy as np
import matplotlib.pyplot as plt
import sys
import random

random.seed()
#Switch on arg[1]. 1: input alpha, 2: input lambda, 3: input a
#Find parameters from alpha
for i in range(1000):
    value1 = random.uniform(-1,1)
    value2 = random.uniform(-1,1)
    #print value1, value2
    alpha1 = value1
    alpha2 = value2
    y1 = (-alpha1 + math.sqrt(alpha1**2. + 4.*alpha2))/(2.*alpha2)
    y2 = (-alpha1 - math.sqrt(alpha1**2. + 4.*alpha2))/(2.*alpha2)
    a1 = (alpha1 / (1-alpha2) - 1/y2) / (1/y1 - 1/y2)
    a2 = (1/y1 - alpha1 / (1-alpha2)) / (1/y1 - 1/y2)
    if alpha1 + alpha2 < 1:
        if alpha2 - alpha1 < 1:
            if abs(alpha2) < 1:
                print "%.5f" % alpha1 + " " + "%.5f" % alpha2 + " " + "%.5f" % (y1) + " " + "%.5f" % (y2) + " " + "%.5f" % a1 + " " + "%.5f" % a2
        
    #find parameters from lambda
    
    lambda1 = value1
    lambda2 = value2
    y1      = math.exp(lambda1)
    y2      = math.exp(lambda2)
    alpha2  = (1-y2/y1)/(y2**2 - y2**3)
    alpha1  = (1-alpha2 * y2**2)/y1
    rho     = alpha1 / (1-alpha2)
    a1      = (rho - 1/y2) / (1/y1 - 1/y2)
    a2      = (1/y1 - rho) / (1/y1 - 1/y2)
    if alpha1 + alpha2 < 1:
        if alpha2 - alpha1 < 1:
            if abs(alpha2) < 1:
                print "%.5f" % alpha1 + " " + "%.5f" % alpha2 + " " + "%.5f" % y1 + " " + "%.5f" % y2 + " " + "%.5f" % a1 + " " + "%.5f" % a2
