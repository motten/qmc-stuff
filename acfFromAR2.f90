program acfFromAR2
  !This program takes in data, and finds the error in 3 ways: AR, Blocking, NL.
  implicit none
  integer, parameter :: r8b =  SELECTED_REAL_KIND(15),p=4 ! 8 bytes = 64 bits (double precision what we usually use)
  integer, parameter :: maxDataPoints = 10000000
  integer            :: i,j,k,printStyle,numberOfDataPoints,numberSkip,m
  real(r8b)          :: data(maxDataPoints),dataTotal,mean,NeffAR2,NeffAR1,&
       &sigma,distanceFromMeanSquared,numberReal,blockError,NeffAR3,&
       &NeffNL,acf(p),dataSum(p),dataSumLag(p),dataAAT(p),meanLag(2),&
       &theoreticalBlockError,numberInBlock,weights(maxDataPoints),&
       &xy(25,2),alpha1,x(3),n_d
  character(len=255) dataFile,arg
  !read the arguments in
  call getarg(1,dataFile)
  call getarg(2,arg)
  read(arg,*) printStyle
  call getarg(3,arg)
  read(arg,*) numberOfDataPoints
  call getarg(4,arg)
  read(arg,*) numberSkip

  !Find a better way to read in the file without knowing the number
  ! of dataPoints
  open(unit=1,file=dataFile,form="formatted",status="old",action&
       &="read")
  data = 0
  !Read the data into a vector
  do i=1,numberSkip
     read(unit=1,fmt=*)
  end do
  do i=1,numberOfDataPoints
     read(1,*) sigma,sigma,weights(i),data(i)
     !read(1,fmt=*) data(i)
  end do
  
  !calculate the mean from the data
  dataTotal  = sum(data)
  numberReal = numberOfDataPoints
  mean       = dataTotal/numberReal
  !print *, 'Mean', mean

  !calculate the standard deviation
  distanceFromMeanSquared = 0
  do i=1,numberOfDataPoints
     distanceFromMeanSquared = distanceFromMeanSquared + (data(i) - mean)**2
  end do

  sigma = sqrt(1/(numberReal-1) * distanceFromMeanSquared)

  !find the autocovariance matrix
  !A^TA, where A is made by Aij is the ith data point at lag j
  !find the autocovariance matrix
  
  dataSum = 0
  dataSumLag = 0
  dataAAT = 0
  do i=1,p
     meanLag(1) = sum(data(i+1:))/(numberOfDataPoints - i)
     meanLag(2) = sum(data(1:(numberOfDataPoints-i)))/(numberOfDataPoints - i)
     
     do j=1+i,numberOfDataPoints
        dataAAT(i) = dataAAT(i) + (data(j)-meanLag(1))*(data(j-i)-meanLag(2))
        dataSum(i) = dataSum(i) + (data(j)-meanLag(1))**2
        dataSumLag(i) = dataSumLag(i) + (data(j-i)-meanLag(2))**2
     end do
  end do

  !dataAAT = matrixA(1,1)
  do i=1,p
     acf(i) = dataAAT(i) / sqrt(dataSum(i) * dataSumLag(i))
  end do

  !vectorB = vectorB / (autoCovariance(1,1))
  NeffAR1 = ar1Neff(data)
  NeffAR2 = ar2Neff(data)
  NeffAR3 = ar3Neff(data)
  NeffNL  = nonLinearNeff()
  if(NeffNL < 0) then
     NeffNL = NeffAR1
  end if
  if(NeffAR3 < 0) then
     NeffAR3 = NeffAR1
  end if
  if(printStyle == 1) then
     write(6,'(''AR(1) Error'',e14.6)') sigma/sqrt(max(NeffAR1-1,1.))
     write(6,'(''AR(2) Error'',e14.6)') sigma/sqrt(max(NeffAR2-1,1.))
     write(6,'(''AR(3) Error'',e14.6)') sigma/sqrt(max(NeffAR3-1,1.))
     write(6,'(''NL Error'',e14.6)') sigma/sqrt(max(NeffNL-1,1.))
     write(6,'(''Variance'',e14.6)') sigma**2
     write(6,'(''acf'',4e14.6)') acf

  else
     print *,'-1',sigma/sqrt(max(NeffAR1-1,1.))
     print *,'-2',sigma/sqrt(max(NeffAR2-1,1.)) 
     print *,'-3',sigma/sqrt(max(NeffAR3-1,1.))
     print *,'-4',sigma/sqrt(max(NeffNL-1,1.))
     !calculate the error from block method
     i=2
     j=1
     data           = data - mean
     blockError     = 0
     do
        if(i>numberOfDataPoints) exit
        blockError = blockingError(i,data)
        xy(j,2) = (blockError**2)/(sigma**2/numberOfDataPoints)
        xy(j,1) = i
        i = i*2
        j = j+1
     end do
     i=2
     m=1
     alpha1 = nonLinearTheory(j-1)
     call nonLinearTheory2(j-1,x)
     !print *,x
     do
        if(i>numberReal) exit
        numberInBlock = numberReal/i
        !alpha1 = nonLinearTheory(m)
        !n_d = numberReal
        !x(1)   = alpha1
        !print *,(xy(1:m,1) - 1 - 2 * x(1)**(n_d/xy(1:m,1))/(1 - x(1)**(n_d/xy(1:m,1))) + 2*x(1)**(n_d/(xy(1:m,1)))&
         !    &*(1-x(1)**(n_d))/(xy(1:m,1)*(1-x(1)**(n_d/xy(1:m,1)))**2))&
          !   &*(1 + 2*x(1)/(1-x(1)) - 2*x(1)*(1-x(1)**(n_d/xy(1:m,1)))/(n_d/xy(1:m,1)*(1-x(1))**2))&
           !  &/(n_d/xy(1:m,1)*(n_d - 1 - 2*x(1)/(1-x(1)) + 2 * x(1) * (1-x(1)**n_d)/(n_d * (1-x(1))**2)))
        !print *,'data',xy(1:m,2)
        !print *,'data2',xy(1:m,1)
        !print *,'rho',alpha1
        m = m+1
        theoreticalBlockError = theoreticalBlockingError(alpha1,numberInBlock)
        print *,int(numberInBlock),sqrt(theoreticalBlockError)
        i = i*2
     end do
     i = 2
     do
        if(i>numberReal) exit
        numberInBlock = numberReal/i
        theoreticalBlockError = theoreticalBlockingError2(x,numberInBlock)
        !print *,x
        print *,int(numberInBlock),sqrt(theoreticalBlockError)
        i=i*2
     end do

  end if

  

contains
  !this function calculates the Neff from an ar(1) model
  real function ar1Neff(data)
    implicit none
    real(r8b), intent(in):: data(numberOfDataPoints)
    integer, parameter   :: p = 1 !the rank
    integer              :: i,j,k
    real(r8b)        :: ar1Alpha,tcorrAR1,capitalTCorrAR1,NeffAR1,vectorB(p),matrixA(p,p)

    call welfordAlgorithm(p,data,matrixA,vectorB,.true.)
    !for AR(1),
    !alpha1 = (A^T*b)_1 / (A^T*A)_11
    ar1Alpha = vectorB(1)/matrixA(1,1)
    !the acf for an ar(1) model is alpha^(-|tau|)
    tcorrAR1        = ar1Alpha/(1-ar1Alpha)
    capitalTCorrAR1 = 1 + 2*tcorrAR1
      NeffAR1         = numberOfDataPoints / capitalTCorrAR1
    
    if(printStyle == 1) then
       write(6,'(''AR(1) Alpha'',e15.6,'' Lambda'',F10.6,'' Neff'',F15.3,'' tcorr'',F15.6,'' TCorr'',F15.6)') ar1Alpha,&
            &(-log(ar1Alpha)),NeffAR1,tcorrAR1,capitalTCorrAR1
    end if
    ar1Neff = NeffAr1

  end function ar1Neff

  !Calculates Neff for an AR(2) model
  real function ar2Neff(data)
    implicit none
    real(r8b), intent(in):: data(numberOfDataPoints)
    integer, parameter  :: p = 2 !rank
    integer                i,j,k
    real(r8b)       :: matrixA(p,p),vectorB(p),tcorr,capitalTCorr
    real(r8b)       :: alpha1,alpha2,y1,y2,a1,a2,Neff

    call welfordAlgorithm(p,data,matrixA,vectorB,.true.)

    !Calculate the AR(2) parameters, alpha1 and alpha2
    !(from 2 equation, 2 unknown elimination technique)
    !alpha2 = ((A^T * b)_2 - (A^T*A)_21/(A^T*A)_11 * (A^T*b)_1)/ 
    !           (A^T*A)_22 - (A^T*A)_21 * (A^T*A)_12 / (A^T*A)_11
    !alpha1 = ((A^T * b)_2 - (A^T*A)_22/(A^T*A)_12 * (A^T*b)_1)/ 
    !           (A^T*A)_21 - (A^T*A)_22 * (A^T*A)_11 / (A^T*A)_12
    
    alpha1 = (vectorB(2) - matrixA(2,2) / matrixA(1,2) * vectorB(1))&
         /(matrixA(2,1) - matrixA(2,2) * matrixA(1,1) / matrixA(1,2))
    alpha2 = (vectorB(2) - matrixA(2,1) / matrixA(1,1) * vectorB(1))&
         /(matrixA(2,2) - matrixA(2,1) * matrixA(1,2) / matrixA(1,1))
    
    !calculate the roots of the characteristic polynomial
    !Phi = 1 - alpha1*y - alpha2*y^2
    !y_j = (-alpha1 - (-1)^j *sqrt(alpha1^2 + 4alpha2))/(2alpha2)
    
    y1 = (-alpha1 + sqrt(alpha1**2 + 4*alpha2))/(2*alpha2)
    y2 = (-alpha1 - sqrt(alpha1**2 + 4*alpha2))/(2*alpha2)
    
    !calculate the prefactors a1 and a2
    a1 = (alpha1/(1 - alpha2) - 1/y2) / (1/y1 - 1/y2)
    a2 = (1/y1 - alpha1/(1-alpha2))/(1/y1 - 1/y2)
    
    !The ACF for an AR(2) model is:
    !rho = a1*y1^(-|tau|) + a2*y2^(-|tau|)
    !integral(rho)= -a1*y1^(-|tau|)/log(y1) - a2*y2^(-|tau|)/log(y2)
    !calculate tcorr,TCorr, and Neff
    !Use a geometric series to calculate the sum
    tcorr        = a1*1/y1/(1-1/y1) + a2*1/y2/(1-1/y2)
    capitalTCorr = 1 + 2 * tcorr
    Neff         = numberOfDataPoints / capitalTCorr
    if(printStyle == 1) then
       write(6,'(''AR(2) Alpha1'',es15.6,'' Alpha2'',es15.6,'' a1'',es15.6,'' a2'',es15.6,'' y1'',F15.6,'' y2'',F15.4)') alpha1,&
            &alpha2,a1,a2,y1,y2
       write(6,'(''AR(2) Neff'',F15.3,'' tcorr'',F15.6,'' TCorr'',F15.6)') Neff,tcorr,capitalTCorr
       write(6,'(''Welford A'',4es15.7)') matrixA/(numberOfDataPoints-p-1)
       write(6,'(''Welford B'',2es15.7)') vectorB/(numberOfDataPoints-p-1)

    end if

    ar2Neff = Neff

  end function ar2Neff

  real function ar3Neff(data)
    implicit none
    real(r8b), intent(in):: data(numberOfDataPoints)
    integer, parameter   :: p = 3 !rank
    integer                 i,j,k
    real(r8b)        :: matrixA(p,p),vectorB(p),alpha1,alpha2,alpha3,Neff,rho1,rho2
    complex(r8b)      :: cubicIntermediate,delta0,delta1,u1,u2,u3,x1,x2,x3,tcorr,capitalTCorr,a1,a2,a3

    call welfordAlgorithm(p,data,matrixA,vectorB,.true.)

    !The normal equations are:
    !(A^T * A) * x = A^T * b
    !where A is made by A_i1 = X_t-1,A_i2 = X_t-2, A_i3 = X_t-3, b_i = X_t, and x
    ! = alpha1, alpha2, alpha3
    !start at i = 4, so that you have the correct number of lags
    !i-j is for the lags
    !find the autocovariance matrix
    !Solve for alpha via the normal equations
    alpha3 = (vectorB(3) - (matrixA(3,1)*vectorB(1))/matrixA(1,1)&
         & + ((matrixA(3,1)*matrixA(1,2))/matrixA(1,1) - &
         &matrixA(3,2))*((vectorB(2) - (matrixA(2,1)*vectorB(1))&
         &/matrixA(1,1))/(matrixA(2,2) - (matrixA(2,1)*&
         &matrixA(1,3))/matrixA(1,1))))/(matrixA(3,3)-&
         &(matrixA(3,1)*matrixA(1,3))/matrixA(1,1) + (matrixA(3,2)&
         & - (matrixA(3,1)*matrixA(1,2))/matrixA(1,1))*&
         &((matrixA(1,3)*matrixA(2,1))/matrixA(1,1) - &
         &matrixA(2,3))/(matrixA(2,2) - (matrixA(2,1)*&
         &matrixA(1,3))/matrixA(1,1)))

    alpha2 = (vectorB(2) - matrixA(2,3)*alpha3 - (matrixA(2,1)*&
         &(vectorB(1) - matrixA(1,3)*alpha3))/matrixA(1,1))/&
         &(matrixA(2,2) - (matrixA(2,1)*matrixA(1,2))/matrixA(1,1))

    alpha1 = (vectorB(1) - matrixA(1,2)*alpha2-matrixA(1,3)*alpha3)/matrixA(1,1) 

    !calculate the roots of the characteristic polynomial
    !Phi = 1 - alpha1*x - alpha2*x^2 - alpha3*x^3
    !Use the cubic formula to find x1,x2,x3    
    u1                = dcmplx(1.0,0.0)
    u2                = dcmplx(-0.5,sqrt(3.)/2)
    u3                = dcmplx(-0.5,-sqrt(3.)/2)
    delta1            = -2*alpha2**3. + 9 * alpha1 * alpha2 * alpha3 + 27 * alpha3**2.
    delta0            = alpha2**2. - 3*alpha3*alpha1
    cubicIntermediate = ((delta1 + sqrt(delta1**2. - 4 * delta0**3.))/2)**(1.0/3.0)
    x1                = (-alpha2 + u1 * cubicIntermediate + delta0 / (u1 * cubicIntermediate))/(3 * alpha3)
    x2                = (-alpha2 + u2 * cubicIntermediate + delta0 / (u2 * cubicIntermediate))/(3 * alpha3)
    x3                = (-alpha2 + u3 * cubicIntermediate + delta0 / (u3 * cubicIntermediate))/(3 * alpha3)
    

    !calculate the prefactors a1, a2, a3
    !first do an intermediate calculation of rho(1) and rho(2)
    rho1 = (alpha1 + alpha2 * alpha3)/(1 - alpha2 - alpha1*alpha3 - alpha3**2.)
    rho2 = ((alpha1 + alpha3)*alpha1 + (1-alpha2)*alpha2)/(1 - alpha2 - alpha1*alpha3 - alpha3**2.)
    a3   = (rho2 - 1/x1**2. + (rho1 - 1/x1)/(1/x2 - 1/x1)*(1/x1**2. - 1/x2**2.))/&
         &(1/x3**2. - 1/x1**2. + (1/x3 - 1/x1)/(1/x2 - 1/x1)*(1/x1**2. - 1/x2**2.))
    a2   = (rho1 - 1/x1 - a3*(1/x3 - 1/x1))/(1/x2-1/x1)
    a1   = 1-a2-a3
 
 !The ACF for an AR(2) model is:
    !rho = a1*x1^(-|tau|) + a2*x2^(-|tau|) + a3*x3^(-|tau|)
    !integral(rho)= -a1*x1^(-|tau|)/log(x1) - a2*x2^(-|tau|)/log(x2)
    !-a3*x3^(-|tau|)/log(x3)
    !calculate tcorr,TCorr, and Neff
    !Use a geometric series to calculate the sum

    tcorr        = (a1 * 1/x1 / (1-1/x1) + a2 * 1/x2/(1-1/x2) + a3 * 1/x3/(1-1/x3))
    capitalTCorr = 1 + 2 * tcorr
    Neff         = numberOfDataPoints / capitalTCorr    

    if(printStyle == 1) then
       write(6,'(''AR(3) Alpha1'',F10.6,'' Alpha2'',F10.6,'' Alpha3'',F10.6)') alpha1,alpha2,alpha3
       write(6,'(''AR(3) a1'',2G15.6,'' a2'',2G15.6,'' a3'',2G15.6)') a1,a2,a3
       write(6,'(''AR(3) x1'',2G15.6,'' x2'',2G15.6,'' x3'',2G15.6)') x1,x2,x3
       write(6,'(''AR(3) Neff'',F15.3,'' tcorr'',F15.6,'' Tcorr'',F15.6)') Neff,real(tcorr),real(capitalTCorr)
    end if

    ar3Neff = Neff

  end function ar3Neff

  !this function calculates the error from using the blocking method
  real function blockingError(nblocks,data)
    implicit none
    real(r8b), intent(in) :: data(numberOfDataPoints)
    integer, intent(in)  :: nblocks
    real                    numberBlocksReal,dataBlockTotal(nblocks),meanBlock(nblocks),&
         &distanceBlockMeanSquared,sigmaBlock,totalMean,totalBlockData
    integer                l,m,numberInBlock
    !calculate the number in each block
    numberInBlock = numberOfDataPoints / nblocks
    do l=1,nblocks
       dataBlockTotal(l) = 0
    end do
    !calculate the mean of each block from the data
    do l=1,nblocks
       do m=1,numberInBlock
          dataBlockTotal(l) = dataBlockTotal(l)+data((l-1)*numberInBlock + m)
       end do
    end do
    do l=1,nblocks
       meanBlock(l) = dataBlockTotal(l) / numberInBlock
    end do
    !print *, 'Mean', mean
    !calculate the mean of all the blocks together
    totalBlockData = 0
    do l=1,nblocks
       totalBlockData = totalBlockData + meanBlock(l)
    end do
    totalMean                = totalBlockData/nblocks
    distanceBlockMeanSquared = 0
    !calculate the standard deviation
    do l=1,nblocks
       distanceBlockMeanSquared = distanceBlockMeanSquared + (meanBlock(l) - totalMean)**2
    end do
    numberBlocksReal = nblocks
    sigmaBlock = sqrt(1/numberBlocksReal * distanceBlockMeanSquared)
    blockingError = sigmaBlock/sqrt(numberBlocksReal - 1)
    if(printStyle /= 1) then
       print *,numberInBlock,blockingError
    end if
  end function blockingError

  real function theoreticalBlockingError(alpha1,N_s)

    real(r8b),intent(in) :: alpha1,N_s
    real(r8b)            :: acfSum
    integer              :: i
    !Do the sum:
    !sum_t=1^(N_s-1) rho(t) * (1 - t/N_s)
    !     = alpha1 / (1 - alpha1) * (1 - (1 - alpha^N)/(N * (1 - alpha)))
    acfSum = alpha1 / (1-alpha1) * (1 - (1-alpha1**N_s)/(N_s * (1 - alpha1)))
    
    theoreticalBlockingError = sigma**2 / numberOfDataPoints * (1 + 2*acfSum) 

  end function theoreticalBlockingError

  real function theoreticalBlockingError2(x,N_s)

    real(r8b),intent(in) :: x(3),N_s
    real(r8b)            :: acfSum
    integer              :: i
    !Do the sum:
    !sum_t=1^(N_s-1) rho(t) * (1 - t/N_s)
    !     = alpha1 / (1 - alpha1) * (1 - (1 - alpha^N)/(N * (1 - alpha)))
    acfSum = x(3) * (x(1) / (1-x(1)) * (1 - (1-x(1)**N_s)/(N_s * (1 - x(1))))) &
         &+ (1-x(3))*(x(2) / (1-x(2)) * (1 - (1-x(2)**N_s)/(N_s * (1 - x(2)))))
    
    theoreticalBlockingError2 = sigma**2 / numberOfDataPoints * (1 + 2*acfSum) 

  end function theoreticalBlockingError2

  !Does a nonlinear leastsquares fit, using minpack, and returns the Neff
  !from the derived ACF
  real function nonLinearNeff()
    
    integer, parameter :: numberOfParameters = 3
    real(kind=8)       :: fvec(numberOfParameters),fjac(numberOfParameters,numberOfParameters)
    integer (kind = 4) :: iflag
    integer (kind = 4) :: info, i
    real (kind = 8)    :: tol,tcorr,capitalTCorr,Neff
    real (kind = 8)    :: x(numberOfParameters)

    !put in initial guess for parameters
    x(1:3) = (/ 0.5, 1.0, 0.01/)
    !set tolerance
    tol = 0.00001

    call lmder1 (functionToFit,numberOfParameters,numberOfParameters,x,fvec,fjac,numberOfParameters,tol,info)
    !print *,"RMS",sqrt(sum(fvec-acf(1))**2/2)
    !print *, fvec,info
    !calculate tcorr,TCorr, and Neff
    tcorr        = x(1)/x(2) + (1-x(1))/x(3)
    capitalTCorr = 1 + 2 * tcorr
    Neff         = numberOfDataPoints / capitalTCorr    

    if(printStyle == 1) then
       write(6,'(''NL a'',F10.6,'' b'',F10.6,'' c'',F10.6,'' d'',F10.6)') x(1),x(2),(1-x(1)),x(3)
       write(6,'(''NL Neff'',F15.6,'' tcorr'',F15.6,'' TCorr'',F15.6)') Neff,tcorr,capitalTCorr
    end if
    nonLinearNeff = Neff
    

  end function nonLinearNeff 

  !Does a nonlinear leastsquares fit, using minpack, and returns the Neff
  !from the derived ACF
  real function nonLinearTheory(m)
    
    integer, parameter :: numberOfParameters = 1
    integer, intent(in):: m
    real(kind=8)       :: fvec(m)
    integer (kind = 4) :: iflag
    integer (kind = 4) :: info, i
    real (kind = 8)    :: tol,tcorr,capitalTCorr,Neff
    real (kind = 8)    :: x(numberOfParameters)

    !put in initial guess for parameters
    x(1) = 0.9
    !set tolerance
    tol = 0.0000001

    call lmdif1 (correctedBlockingFunctionToFit,m,numberOfParameters,x,fvec,tol,info)
    !print *,"RMS",sqrt(sum(fvec-xy(1:m,2))**2/2)
    !print *,'info', info

    nonLinearTheory = x(1)
    
  end function nonLinearTheory

  subroutine nonLinearTheory2(m,x)
    
    integer, parameter :: numberOfParameters = 3
    integer, intent(in):: m
    real(kind=8)       :: fvec(m)
    integer (kind = 4) :: iflag
    integer (kind = 4) :: info, i
    real (kind = 8)    :: tol,tcorr,capitalTCorr,Neff
    real (kind = 8),intent(inout)    :: x(numberOfParameters)

    !put in initial guess for parameters
    x(1:3) = (/ 0.9, 0.9, 0.9 /)
    !set tolerance
    tol = 0.0000001

    call lmdif1 (correctedBlockingFunction2,m,numberOfParameters,x,fvec,tol,info)
    !print *,"RMS",sqrt(sum(fvec-xy(1:m,2))**2/2)
    !print *,'info', info
    
  end subroutine nonLinearTheory2
  
  !This subroutine is used in the minpack nonlinear fit
  !The following comment is from minpack.f90, explaining the parameters
  !subroutine fcn ( m, n, x, fvec, fjac, ldfjac, iflag )
!      integer ( kind = 4 ) ldfjac
!      integer ( kind = 4 ) n
!      real fjac(ldfjac,n)
!      real fvec(m)
!      integer ( kind = 4 ) iflag
!      real x(n)
!
!    If IFLAG = 1 on intput, FCN should calculate the functions at X and
!    return this vector in FVEC.
!    If IFLAG = 2 on input, FCN should calculate the jacobian at X and
!    return this matrix in FJAC.
!    To terminate the algorithm, the user may set IFLAG negative.
!
!    Input, integer ( kind = 4 ) M, the number of functions.
!
!    Input, integer ( kind = 4 ) N, is the number of variables.  
!    N must not exceed M.
!
!    Input/output, real ( kind = 8 ) X(N).  On input, X must contain an initial
!    estimate of the solution vector.  On output X contains the final
!    estimate of the solution vector.
!
!    Output, real ( kind = 8 ) FVEC(M), the functions evaluated at the output X.
!
!    Output, real ( kind = 8 ) FJAC(LDFJAC,N), an M by N array.  The upper
!    N by N submatrix contains an upper triangular matrix R with
!    diagonal elements of nonincreasing magnitude such that
!      P' * ( JAC' * JAC ) * P = R' * R,
!    where P is a permutation matrix and JAC is the final calculated
!    jacobian.  Column J of P is column IPVT(J) of the identity matrix.
!    The lower trapezoidal part of FJAC contains information generated during
!    the computation of R.
!
!    Input, integer ( kind = 4 ) LDFJAC, is the leading dimension of FJAC,
!    which must be no less than M.
  subroutine functionToFit(m,n,x,fvec,fjac,ldfjac,iflag)
    implicit none

    integer (kind = 4) :: ldfjac,m,n,iflag,i
    real (kind=8)      :: x(n),fjac(ldfjac,n),fvec(m),dataIndex(3),autoCovarianceData(3)
    
    do i=1,3
       dataIndex(i)          = i
       autoCovarianceData(i) = acf(i)
    end do
    

    if(iflag==1) then
       fvec(1:m) = x(1)*exp(-x(2) * dataIndex(1:m)) + (1-x(1))*exp(-x(3) * &
            &dataIndex(1:m)) - autoCovarianceData(1:m)
    else if(iflag==2) then
       fjac(1:m,1) = exp(-x(2)*dataIndex(1:m)) - exp(-x(3)*dataIndex(1:m))
       fjac(1:m,2) = -x(1) * dataIndex(1:m) * exp(-x(2) * dataIndex(1:m))
       fjac(1:m,3) = (x(1) - 1) * dataIndex(1:m) * exp(-x(3) * dataIndex(1:m))
    end if
  end subroutine functionToFit

  subroutine correctedBlockingFunctionToFit(m,n,x,fvec,iflag)
    implicit none

    integer (kind = 4) :: m,n,iflag,j
    real (kind=8)      :: x(n),fvec(m),n_d,v(m),&
         &w(m),l(m),TcorrN,TcorrNb(m),TcorrNs(m),T
    n_d = numberOfDataPoints

    TcorrN  = 1 + ((2*x(1))/(1-x(1)))*(1 - (1-x(1)**n_d)/(n_d*(1-x(1))))
    TcorrNb = 1 + (2 * x(1)**(n_d/xy(1:m,1))/(1-x(1)**(n_d/xy(1:m,1))))*&
         &(1 - (1-x(1)**(n_d))/(xy(1:m,1)*(1-x(1)**(n_d/xy(1:m,1)))))
    TcorrNs = 1 + (2*x(1))/(1-x(1))*(1 - (1-x(1)**(n_d/xy(1:m,1)))&
         &/(n_d/xy(1:m,1)*(1-x(1))))
    
    v(1:m)  = 1/(n_d/xy(1:m,1)*(n_d - TcorrN))
    w(1:m) = xy(1:m,1) - TcorrNb
    l(1:m) = TcorrNs


    !fvec(1:m) = ((xy(1:m,1) - 1 - 2 * x(1)**(n_d/xy(1:m,1))/(1 - x(1)**(n_d/xy(1:m,1))) + 2*x(1)**(n_d/(xy(1:m,1)))&
     !    &*(1-x(1)**(n_d))/(xy(1:m,1)*(1-x(1)**(n_d/xy(1:m,1)))**2))&
      !   &*(1 + 2*x(1)/(1-x(1)) - 2*x(1)*(1-x(1)**(n_d/xy(1:m,1)))/(n_d/xy(1:m,1)*(1-x(1))**2))&
       !  &/(n_d/xy(1:m,1)*(n_d - 1 - 2*x(1)/(1-x(1)) + 2 * x(1) * (1-x(1)**n_d)/(n_d * (1-x(1))**2)))) - xy(1:m,2)
    fvec(1:m) = TcorrNs - xy(1:m,2)
   
  end subroutine correctedBlockingFunctionToFit

 subroutine correctedBlockingFunction2(m,n,x,fvec,iflag)
    implicit none

    integer (kind = 4) :: m,n,iflag
    real (kind=8)      :: x(n),fvec(m),n_d,v(m),&
         &w(m),l(m),TcorrN,TcorrNs2(m),TcorrNs(m),T
    n_d = numberOfDataPoints

    TcorrNs   = x(3) * (x(1)/(1-x(1)) * (1 - (1-x(1)**(n_d/xy(1:m,1)))&
         &/(n_d/xy(1:m,1)*(1-x(1))))) + (1-x(3)) * (x(2)/(1-x(2)) *&
         &(1 - (1-x(2)**(n_d/xy(1:m,1)))/(n_d/xy(1:m,1)*(1-x(2)))))
    fvec(1:m) = 1 + 2*TcorrNS - xy(1:m,2)
   
  end subroutine correctedBlockingFunction2

  !Calculates matrixA and vectorB (the covariances) to be used
  !in the normal equations via the welfordAlgorithm
  !Arguments: p - rank
  !           data - the data
  !           matrixA
  !           vectorB
  subroutine welfordAlgorithm(p,data,matrixAs,vectorBs,backwards)
    
    integer, intent(in)      :: p !rank
    logical, intent(in)      :: backwards
    real(r8b), intent(in)    :: data(numberOfDataPoints)
    real(r8b), intent(inout) :: matrixAs(p,p),vectorBs(p)
    integer                  :: i,j,k
    real(r8b)                :: meansA(0:p),deltas(0:p)
    
    !Welford algortihm
    !do the forward implementation
    meansA   = 0
    matrixAs = 0
    vectorBs = 0
    do k=p+1,numberOfDataPoints
       do i=0,p
          deltas(i)         = data(k-i) - meansA(i)
          meansA(i)         = meansA(i) + deltas(i)/(k-p)
          if(i>=1) then
             vectorBs(i) = vectorBs(i) + deltas(0) * (data(k-i) - meansA(i))
             do j=1,i
             matrixAs(i,j) = matrixAs(i,j) + deltas(j) *(data(k-i) - meansA(i))
                 if(j.ne.i) matrixAs(j,i) = matrixAs(j,i) + deltas(i) *(data(k-j) - meansA(j))
             end do
           end if
        end do
     end do
     
     !now run backwards
     if(backwards.eqv..true.) then
        do k=p+1,numberOfDataPoints
           do i=0,p
              deltas(i)         = data(k-i) - meansA(i)
              meansA(i)         = meansA(i) + deltas(i)/(k+numberOfDataPoints-p)
              if(i>=1) then
                 vectorBs(i) = vectorBs(i) + deltas(p) * (data(k+i-p) - meansA(p-i))
                 do j=1,i
                    matrixAs(i,j) = matrixAs(i,j) + deltas(p-j) *(data(k+i-p) - meansA(p-i))
                    if(j.ne.i) then
                       matrixAs(j,i) = matrixAs(j,i) + deltas(p-i) *(data(k+j-p) - meansA(p-j))
                    end if
                 end do
              end if
           end do
        end do
        vectorBs = vectorBs / 2.
        matrixAs = matrixAs / 2.
     end if
        
        
    !print *, "matrix A", matrixAs
    !print *, "vector B", vectorBs
    !do the backward implementation
    

    !print *, "matrix A", matrixAs
    !print *, "vector B", vectorBs
  end subroutine welfordAlgorithm
end program acfFromAR2
