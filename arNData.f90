program arNData
  !This program creates fictitious data for an AR(10) model
  IMPLICIT NONE
  !Set the number of data points here
  integer, parameter :: numberOfDataPoints = 2**20, rank=10 !Turn these into arguments
  integer            :: time_info(8),seed,n,i,j,numberOfArguments
  real                  dataPoint,dataPointLag(rank),dataPointLag2,&
       &alpha(rank),mean,whiteNoise,y1,y2
  character(len=255)    arg
  !OPEN file
  open(UNIT=1,FILE="ar2Data.dat",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")
  !Allow command line input
  !read the arguments in
  do i=1,rank
     call getarg(i,arg)
     read(arg,*) alpha(i)
  end do

  call date_and_time(VALUES = time_info)
  !Set a randomish seed 
  seed = 1000 * time_info(7) + time_info(8)
  call random_seed(SIZE = n)
  call random_seed(PUT = (/ (i *seed, i = 1, n) /) )

  mean   = 12.345
  !AR(2) equation: X_t = mean + sum(alphai * (X_t-i - mean)) + whiteNoise_t
  do i=1,rank
     call random_number(dataPointLag(i))
  end do
  do i = 1, numberOfDataPoints
     !Generate some white noise
     !The whiteNoise is subtracted by 0.5
     !in the equation because we want a mean of 0, and random_number gives a
     !mean of 0.5
     call random_number(whiteNoise)
     dataPoint = mean + whiteNoise - 0.5
     do j=1,rank
        dataPoint = dataPoint + alpha(j) * (dataPointLag(j) - mean)
     end do
     !print *, whiteNoise
     write(UNIT=1,FMT=*) dataPoint
     do j=rank,2,-1
        dataPointLag(j) = dataPointLag(j-1)
     end do
     dataPointLag(1) = dataPoint
  end do

  close(UNIT=1)

end program arNData
