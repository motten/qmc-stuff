program acfFromAR2
  !This program takes in data, which is presumed to be AR(1) or (2) and finds
  !the ACF.
  implicit none
  integer, PARAMETER :: numberOfDataPoints = 2**11
  integer            :: i,j,k,printStyle
  real(kind=10)      :: data(numberOfDataPoints),dataTotal,mean,NeffAR2,NeffAR1,&
       &sigma,distanceFromMeanSquared,numberReal,blockError,NeffAR3,sigma2Z,&
       &zData(numberOfDataPoints),zMean,NeffNL,matrixA(numberOfDataPoints-4,4),&
       &matrixATranspose(4,numberOfDataPoints-4),autoCovariance(4,4),dataNL(numberOfDataPoints-4)
  character(len=255) dataFile,arg
  !read the arguments in
  call getarg(1,dataFile)
  call getarg(2,arg)
  read(arg,*) printStyle

  !Find a better way to read in the file without knowing the number
  ! of dataPoints
  open(unit=1,file=dataFile,form="formatted",status="old",action&
       &="read")
  
  !Read the data into a vector
  do i = 1,numberOfDataPoints
     read(unit=1,fmt=*) data(i)
  end do

  dataTotal=0
  !calculate the mean from the data
  do i=1,numberOfDataPoints
     dataTotal = dataTotal+data(i)
  end do
  numberReal = numberOfDataPoints
  mean       = dataTotal/numberReal
  !print *, 'Mean', mean

  distanceFromMeanSquared = 0
  !calculate the standard deviation
  do i=1,numberOfDataPoints
     distanceFromMeanSquared = distanceFromMeanSquared + (data(i) - mean)**2
     zData(i)                = data(i) - mean
  end do

  sigma = sqrt(1/numberReal * distanceFromMeanSquared)

  !calculate the standard deviation of:
  !zt = Xt - <X>
  dataTotal = 0
  do i=1,numberOfDataPoints
     dataTotal = dataTotal+zData(i)
  end do
  zMean                   = dataTotal/numberReal
  distanceFromMeanSquared = 0
  do i=1,numberOfDataPoints
     distanceFromMeanSquared = distanceFromMeanSquared + (zData(i) - zMean)**2
  end do
  sigma2z = 1/numberReal * distanceFromMeanSquared

  !find the autocovariance matrix
  !A^TA, where A is made by Aij is the ith data point at lag j
  !find the autocovariance matrix
  do i=5,numberOfDataPoints
     dataNL(i-4) = data(i)
     do j=1,4
        matrixA(i-4,j) = data(i-j)
        matrixATranspose(j,i-4) = data(i-j)
     end do
  end do
  
  mean = sum(dataNL)/(numberOfDataPoints-4)
  dataNL = dataNL - mean
  do j=1,4
     mean = sum(matrixA(:,j))/(numberOfDataPoints-4)
     matrixA(:,j) = matrixA(:,j) - mean
     matrixATranspose(j,:) = matrixATranspose(j,:) - mean
  end do
  
  autoCovariance = matmul(matrixATranspose,matrixA)/((numberOfDataPoints-4))
  autoCovariance = autoCovariance / (autoCovariance(1,1))

  NeffAR1 = ar1Neff(data)
  NeffAR2 = ar2Neff(data)
  NeffAR3 = ar3Neff(data)
  NeffNL  = nonLinearNeff()
  if(NeffNL < 0) then
     NeffNL = NeffAR1
  end if
  if(printStyle == 1) then
     write(6,'(''AR(1) Error'',e14.6)') sigma/sqrt(NeffAR1)
     write(6,'(''AR(2) Error'',e14.6)') sigma/sqrt(NeffAR2)
     write(6,'(''AR(3) Error'',e14.6)') sigma/sqrt(NeffAR3)
     write(6,'(''NL Error'',e14.6)') sigma/sqrt(NeffNL)
     write(6,'(''SigmaZ^2'',e14.6)') sigma2z
     write(6,'(''Variance'',e14.6)') sigma**2
     write(6,'(''Autocovariance'',4e14.6)') autoCovariance(1:4,1)
  else
     print *,'-1',sigma/sqrt(NeffAR1)
     print *,'-2',sigma/sqrt(NeffAR2) 
     print *,'-3',sigma/sqrt(NeffAR3)
     print *,'-4',sigma/sqrt(NeffNL)
  end if

  !calculate the error from block method
  i=2
  do
     if(i>numberOfDataPoints) exit
     blockError = blockingError(i,data)
     i = i*2
  end do

contains
  !this function calculates the Neff from an ar(1) model
  real function ar1Neff(data)
    implicit none
    real(10), intent(in):: data(numberOfDataPoints)
    integer          i
    real(kind=10) :: ar1Alpha,tcorrAR1,capitalTCorrAR1,NeffAR1,Atransposeb(1),AtransposeA(1),&
         &vectorA(numberOfDataPoints-1),vectorATranspose(1,numberOfDataPoints-1),&
         &dataAR1(numberOfDataPoints-1),meanAR1

    do i=2,numberOfDataPoints
       dataAR1(i-1) = data(i)
       vectorA(i-1) = data(i-1)
       vectorATranspose(1,i-1) = data(i-1)
    end do
    meanAR1 = sum(dataAR1)/(numberOfDataPoints-1)
    dataAR1 = dataAR1 - meanAR1
    meanAR1 = sum(vectorA)/(numberOfDataPoints-1)
    vectorA = vectorA - meanAR1
    vectorATranspose = vectorATranspose - meanAR1
    !The normal equations are:
    !(A^T * A) * x = A^T * b
    Atransposeb = matmul(vectorATranspose,dataAR1)
    AtransposeA = matmul(vectorATranspose,vectorA)

    !for AR(1),
    !alpha1 = (A^T*b)_2 / (A^T*A)_22
    ar1Alpha = Atransposeb(1)/AtransposeA(1)

    !the acf for an ar(1) model is alpha^(-|tau|)
    tcorrAR1        = -1 / log(ar1Alpha)
    capitalTCorrAR1 = 1 + 2*tcorrAR1
    NeffAR1         = numberOfDataPoints / capitalTCorrAR1
    
    if(printStyle == 1) then
       write(6,'(''AR(1) Alpha'',e15.6,'' Lambda'',F10.6,'' Neff'',F15.3,'' tcorr'',F15.6,'' TCorr'',F15.6)') ar1Alpha,&
            &(-log(ar1Alpha)),NeffAR1,tcorrAR1,capitalTCorrAR1
    end if
    ar1Neff = NeffAr1

  end function ar1Neff

  !Calculates Neff for an AR(2) model
  real function ar2Neff(data)
    implicit none
    real(10), intent(in):: data(numberOfDataPoints)
    integer                i,j,k
    real(kind=10)       :: matrixA(numberOfDataPoints-2,2),matrixATranspose(2,numberOfDatapoints-2),&
         &tcorr,capitalTCorr,dataAR2(numberOfDataPoints-2),meanAR2
    real(kind=10)       :: AtransposeA(2,2),Atransposeb(2),alpha1,alpha2,&
       &y1,y2,a1,a2,Neff

    !The normal equations are:
    !(A^T * A) * x = A^T * b
    !where A is made by A_i1 = X_t-1 and A_i2 = X_t-2, b_i = X_t, and x
    ! = alpha1, alpha2
    !start at i = 3, so that you have the correct number of lags
    !i-j is to get the lags correct

    do i=3,numberOfDataPoints
       dataAR2(i-2) = data(i)
       do j=1,2
          matrixA(i-2,j) = data(i-j)
          matrixATranspose(j,i-2) = data(i-j)
       end do
    end do
    meanAR2 = sum(dataAR2)/(numberOfDataPoints-2)
    dataAR2 = dataAR2 - meanAR2
    meanAR2 = sum(matrixA(:,1))/(numberOfDataPoints-2)
    matrixA(:,1) = matrixA(:,1) - meanAR2
    matrixATranspose(1,:) = matrixATranspose(1,:) - meanAR2
    meanAR2 = sum(matrixA(:,2))/(numberOfDataPoints-2)
    matrixA(:,2) = matrixA(:,2) - meanAR2
    matrixATranspose(2,:) = matrixATranspose(2,:) - meanAR2
    
    ATransposeA = matmul(matrixATranspose,matrixA)
    ATransposeb = matmul(matrixATranspose,dataAR2)
    !Calculate the AR(2) parameters, alpha1 and alpha2
    !(from 2 equation, 2 unknown elimination technique)
    !alpha2 = ((A^T * b)_2 - (A^T*A)_21/(A^T*A)_11 * (A^T*b)_1)/ 
    !           (A^T*A)_22 - (A^T*A)_21 * (A^T*A)_12 / (A^T*A)_11
    !alpha1 = ((A^T * b)_2 - (A^T*A)_22/(A^T*A)_12 * (A^T*b)_1)/ 
    !           (A^T*A)_21 - (A^T*A)_22 * (A^T*A)_11 / (A^T*A)_12
    
    alpha1 = (Atransposeb(2) - AtransposeA(2,2) / AtransposeA(1,2) * Atransposeb(1))&
         /(AtransposeA(2,1) - AtransposeA(2,2) * AtransposeA(1,1) / AtransposeA(1,2))
    alpha2 = (Atransposeb(2) - AtransposeA(2,1) / AtransposeA(1,1) * Atransposeb(1))&
         /(AtransposeA(2,2) - AtransposeA(2,1) * AtransposeA(1,2) / AtransposeA(1,1))
    
    !calculate the roots of the characteristic polynomial
    !Phi = 1 - alpha1*y - alpha2*y^2
    !y_j = (-alpha1 - (-1)^j *sqrt(alpha1^2 + 4alpha2))/(2alpha2)
    
    y1 = (-alpha1 + sqrt(alpha1**2 + 4*alpha2))/(2*alpha2)
    y2 = (-alpha1 - sqrt(alpha1**2 + 4*alpha2))/(2*alpha2)
    
    !calculate the prefactors a1 and a2
    a1 = (alpha1/(1 - alpha2) - 1/y2) / (1/y1 - 1/y2)
    a2 = (1/y1 - alpha1/(1-alpha2))/(1/y1 - 1/y2)
    
    !The ACF for an AR(2) model is:
    !rho = a1*y1^(-|tau|) + a2*y2^(-|tau|)
    !integral(rho)= -a1*y1^(-|tau|)/log(y1) - a2*y2^(-|tau|)/log(y2)
    !calculate tcorr,TCorr, and Neff
    !Use a geometric series to calculate the sum
    tcorr        = a1*1/y1/(1-1/y1) + a2*1/y2/(1-1/y2)
    capitalTCorr = 1 + 2 * tcorr
    Neff         = numberOfDataPoints / capitalTCorr
    if(printStyle == 1) then
       write(6,'(''AR(2) Alpha1'',F10.6,'' Alpha2'',F10.6,'' a1'',F10.6,'' a2'',F10.6,'' y1'',F15.6,'' y2'',F15.6)') alpha1,&
            &alpha2,a1,a2,y1,y2
       write(6,'(''AR(2) Neff'',F15.3,'' tcorr'',F15.6,'' TCorr'',F15.6)') Neff,tcorr,capitalTCorr
    end if

    ar2Neff = Neff

  end function ar2Neff

  real function ar3Neff(data)
    implicit none
    real(10), intent(in) :: data(numberOfDataPoints)
    integer                 i,j,k
    real(kind=10)        :: matrixA(numberOfDataPoints-3,3),matrixATranspose(3,numberOfDatapoints-3),&
         &AtransposeA(3,3),Atransposeb(3),alpha1,alpha2,&
         &alpha3,Neff,rho1,rho2,dataAR3(numberOfDataPoints-3),meanAR3
    complex(kind=8)      :: cubicIntermediate,delta0,delta1,u1,u2,u3,x1,x2,x3,tcorr,capitalTCorr,a1,a2,a3
    !The normal equations are:
    !(A^T * A) * x = A^T * b
    !where A is made by A_i1 = X_t-1,A_i2 = X_t-2, A_i3 = X_t-3, b_i = X_t, and x
    ! = alpha1, alpha2, alpha3
    !start at i = 4, so that you have the correct number of lags
    !i-j is for the lags
    !find the autocovariance matrix
    do i=4,numberOfDataPoints
       dataAR3(i-3) = data(i)
       do j=1,3
          matrixA(i-3,j) = data(i-j)
          matrixATranspose(j,i-3) = data(i-j)
       end do
    end do
    meanAR3 = sum(dataAR3)/(numberOfDataPoints-3)
    dataAR3 = dataAR3 - meanAR3
    do j=1,3
       meanAR3 = sum(matrixA(:,j))/(numberOfDataPoints-3)
       matrixA(:,j) = matrixA(:,j) - meanAR3
       matrixATranspose(j,:) = matrixATranspose(j,:) - meanAR3
    end do
    
    !Multiply A^T * A and A^T * b
    ATransposeA = matmul(matrixATranspose,matrixA)
    ATransposeb = matmul(matrixATranspose,dataAR3)
   
    !Solve for alpha via the normal equations
    alpha3 = (Atransposeb(3) - (AtransposeA(3,1)*Atransposeb(1))/AtransposeA(1,1)&
         & + ((AtransposeA(3,1)*AtransposeA(1,2))/AtransposeA(1,1) - &
         &AtransposeA(3,2))*((Atransposeb(2) - (AtransposeA(2,1)*Atransposeb(1))&
         &/AtransposeA(1,1))/(AtransposeA(2,2) - (AtransposeA(2,1)*&
         &AtransposeA(1,3))/AtransposeA(1,1))))/(AtransposeA(3,3)-&
         &(AtransposeA(3,1)*AtransposeA(1,3))/AtransposeA(1,1) + (AtransposeA(3,2)&
         & - (AtransposeA(3,1)*AtransposeA(1,2))/AtransposeA(1,1))*&
         &((AtransposeA(1,3)*AtransposeA(2,1))/AtransposeA(1,1) - &
         &AtransposeA(2,3))/(AtransposeA(2,2) - (AtransposeA(2,1)*&
         &AtransposeA(1,3))/AtransposeA(1,1)))

    alpha2 = (Atransposeb(2) - AtransposeA(2,3)*alpha3 - (AtransposeA(2,1)*&
         &(Atransposeb(1) - AtransposeA(1,3)*alpha3))/AtransposeA(1,1))/&
         &(AtransposeA(2,2) - (AtransposeA(2,1)*AtransposeA(1,2))/AtransposeA(1,1))

    alpha1 = (Atransposeb(1) - AtransposeA(1,2)*alpha2-AtransposeA(1,3)*alpha3)/AtransposeA(1,1) 

    !calculate the roots of the characteristic polynomial
    !Phi = 1 - alpha1*x - alpha2*x^2 - alpha3*x^3
    !Use the cubic formula to find x1,x2,x3    
    u1                = dcmplx(1.0,0.0)
    u2                = dcmplx(-0.5,sqrt(3.)/2)
    u3                = dcmplx(-0.5,-sqrt(3.)/2)
    delta1            = -2*alpha2**3. + 9 * alpha1 * alpha2 * alpha3 + 27 * alpha3**2.
    delta0            = alpha2**2. - 3*alpha3*alpha1
    cubicIntermediate = ((delta1 + sqrt(delta1**2. - 4 * delta0**3.))/2)**(1.0/3.0)
    x1                = (-alpha2 + u1 * cubicIntermediate + delta0 / (u1 * cubicIntermediate))/(3 * alpha3)
    x2                = (-alpha2 + u2 * cubicIntermediate + delta0 / (u2 * cubicIntermediate))/(3 * alpha3)
    x3                = (-alpha2 + u3 * cubicIntermediate + delta0 / (u3 * cubicIntermediate))/(3 * alpha3)
    

    !calculate the prefactors a1, a2, a3
    !first do an intermediate calculation of rho(1) and rho(2)
    rho1 = (alpha1 + alpha2 * alpha3)/(1 - alpha2 - alpha1*alpha3 - alpha3**2.)
    rho2 = ((alpha1 + alpha3)*alpha1 + (1-alpha2)*alpha2)/(1 - alpha2 - alpha1*alpha3 - alpha3**2.)
    a3   = (rho2 - 1/x1**2. + (rho1 - 1/x1)/(1/x2 - 1/x1)*(1/x1**2. - 1/x2**2.))/&
         &(1/x3**2. - 1/x1**2. + (1/x3 - 1/x1)/(1/x2 - 1/x1)*(1/x1**2. - 1/x2**2.))
    a2   = (rho1 - 1/x1 - a3*(1/x3 - 1/x1))/(1/x2-1/x1)
    a1   = 1-a2-a3
 
 !The ACF for an AR(2) model is:
    !rho = a1*x1^(-|tau|) + a2*x2^(-|tau|) + a3*x3^(-|tau|)
    !integral(rho)= -a1*x1^(-|tau|)/log(x1) - a2*x2^(-|tau|)/log(x2)
    !-a3*x3^(-|tau|)/log(x3)
    !calculate tcorr,TCorr, and Neff
    !Use a geometric series to calculate the sum

    tcorr        = (a1 * 1/x1 / (1-1/x1) + a2 * 1/x2/(1-1/x2) + a3 * 1/x3/(1-1/x3))
    capitalTCorr = 1 + 2 * tcorr
    Neff         = numberOfDataPoints / capitalTCorr    

    if(printStyle == 1) then
       write(6,'(''AR(3) Alpha1'',F10.6,'' Alpha2'',F10.6,'' Alpha3'',F10.6)') alpha1,alpha2,alpha3
       write(6,'(''AR(3) a1'',2G15.6,'' a2'',2G15.6,'' a3'',2G15.6)') a1,a2,a3
       write(6,'(''AR(3) x1'',2G15.6,'' x2'',2G15.6,'' x3'',2G15.6)') x1,x2,x3
       write(6,'(''AR(3) Neff'',F15.3,'' tcorr'',F15.6,'' Tcorr'',F15.6)') Neff,real(tcorr),real(capitalTCorr)
    end if

    ar3Neff = Neff

  end function ar3Neff

  !this function calculates the error from using the blocking method
  real function blockingError(nblocks,data)
    implicit none
    real(10), intent(in) :: data(numberOfDataPoints)
    integer, intent(in)  :: nblocks
    real                    numberBlocksReal,dataBlockTotal(nblocks),meanBlock(nblocks),&
         &distanceBlockMeanSquared,sigmaBlock,totalMean,totalBlockData
    integer                l,m,numberInBlock
    !calculate the number in each block
    numberInBlock = numberOfDataPoints / nblocks
    do l=1,nblocks
       dataBlockTotal(l) = 0
    end do
    !calculate the mean of each block from the data
    do l=1,nblocks
       do m=1,numberInBlock
          dataBlockTotal(l) = dataBlockTotal(l)+data((l-1)*numberInBlock + m)
       end do
    end do
    do l=1,nblocks
       meanBlock(l) = dataBlockTotal(l) / numberInBlock
    end do
    !print *, 'Mean', mean
    !calculate the mean of all the blocks together
    totalBlockData = 0
    do l=1,nblocks
       totalBlockData = totalBlockData + meanBlock(l)
    end do
    totalMean                = totalBlockData/nblocks
    distanceBlockMeanSquared = 0
    !calculate the standard deviation
    do l=1,nblocks
       distanceBlockMeanSquared = distanceBlockMeanSquared + (meanBlock(l) - totalMean)**2
    end do
    numberBlocksReal = nblocks
    sigmaBlock = sqrt(1/numberBlocksReal * distanceBlockMeanSquared)
    blockingError = sigmaBlock/sqrt(numberBlocksReal - 1)
    if(printStyle /= 1) then
       print *,numberInBlock,blockingError
    end if
  end function blockingError

  !Does a nonlinear leastsquares fit, using minpack, and returns the Neff
  !from the derived ACF
  real function nonLinearNeff()
    
    integer, parameter :: numberOfParameters = 3
    real(kind=8)       :: fvec(numberOfParameters),fjac(numberOfParameters,numberOfParameters),autoCovarianceData(3)
    integer (kind = 4) :: iflag
    integer (kind = 4) :: info, i
    real (kind = 8)    :: tol,tcorr,capitalTCorr,Neff
    real (kind = 8)    :: x(numberOfParameters)

    !put in initial guess for parameters
    x(1:3) = (/ 0.05, 1.0, 0.01/)
    !set tolerance
    tol = 0.00001

    call lmder1 (functionToFit,numberOfParameters,numberOfParameters,x,fvec,fjac,numberOfParameters,tol,info)
    do i=1,3
       autoCovarianceData(i) = autoCovariance(1,i+1)/sigma**2
    end do
    !print *,"RMS",sqrt(sum(fvec-autoCovarianceData)**2/2)
    !write(6,'(6F10.6,I5)') autoCovarianceData,fvec,info
    !calculate tcorr,TCorr, and Neff
    tcorr        = x(1)/x(2) + (1-x(1))/x(3)
    capitalTCorr = 1 + 2 * tcorr
    Neff         = numberOfDataPoints / capitalTCorr    

    if(printStyle == 1) then
       write(6,'(''NL a'',F10.6,'' b'',F10.6,'' c'',F10.6,'' d'',F10.6)') x(1),x(2),(1-x(1)),x(3)
       write(6,'(''NL Neff'',F15.6,'' tcorr'',F15.6,'' TCorr'',F15.6)') Neff,tcorr,capitalTCorr
    end if
    nonLinearNeff = Neff
    

  end function nonLinearNeff
  
  !This subroutine is used in the minpack nonlinear fit
  !The following comment is from minpack.f90, explaining the parameters
  !subroutine fcn ( m, n, x, fvec, fjac, ldfjac, iflag )
!      integer ( kind = 4 ) ldfjac
!      integer ( kind = 4 ) n
!      real fjac(ldfjac,n)
!      real fvec(m)
!      integer ( kind = 4 ) iflag
!      real x(n)
!
!    If IFLAG = 1 on intput, FCN should calculate the functions at X and
!    return this vector in FVEC.
!    If IFLAG = 2 on input, FCN should calculate the jacobian at X and
!    return this matrix in FJAC.
!    To terminate the algorithm, the user may set IFLAG negative.
!
!    Input, integer ( kind = 4 ) M, the number of functions.
!
!    Input, integer ( kind = 4 ) N, is the number of variables.  
!    N must not exceed M.
!
!    Input/output, real ( kind = 8 ) X(N).  On input, X must contain an initial
!    estimate of the solution vector.  On output X contains the final
!    estimate of the solution vector.
!
!    Output, real ( kind = 8 ) FVEC(M), the functions evaluated at the output X.
!
!    Output, real ( kind = 8 ) FJAC(LDFJAC,N), an M by N array.  The upper
!    N by N submatrix contains an upper triangular matrix R with
!    diagonal elements of nonincreasing magnitude such that
!      P' * ( JAC' * JAC ) * P = R' * R,
!    where P is a permutation matrix and JAC is the final calculated
!    jacobian.  Column J of P is column IPVT(J) of the identity matrix.
!    The lower trapezoidal part of FJAC contains information generated during
!    the computation of R.
!
!    Input, integer ( kind = 4 ) LDFJAC, is the leading dimension of FJAC,
!    which must be no less than M.
  subroutine functionToFit(m,n,x,fvec,fjac,ldfjac,iflag)
    implicit none

    integer (kind = 4) :: ldfjac,m,n,iflag,i
    real (kind=8)      :: x(n),fjac(ldfjac,n),fvec(m),dataIndex(3),autoCovarianceData(3)
    
    do i=1,3
       dataIndex(i)          = i
       autoCovarianceData(i) = autoCovariance(1,i+1)
    end do
    

    if(iflag==1) then
       fvec(1:m) = x(1)*exp(-x(2) * dataIndex(1:m)) + (1-x(1))*exp(-x(3) * &
            &dataIndex(1:m)) - autoCovarianceData(1:m)
    else if(iflag==2) then
       fjac(1:m,1) = exp(-x(2)*dataIndex(1:m)) - exp(-x(3)*dataIndex(1:m))
       fjac(1:m,2) = -x(1) * dataIndex(1:m) * exp(-x(2) * dataIndex(1:m))
       fjac(1:m,3) = (x(1) - 1) * dataIndex(1:m) * exp(-x(3) * dataIndex(1:m))
    end if
  end subroutine functionToFit
end program acfFromAR2
